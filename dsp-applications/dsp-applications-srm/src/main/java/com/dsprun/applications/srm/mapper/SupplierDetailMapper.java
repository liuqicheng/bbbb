package com.dsprun.applications.srm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dsprun.applications.srm.model.SupplierDetail;
import org.apache.ibatis.annotations.Mapper;import java.util.List;

@Mapper
public interface SupplierDetailMapper extends BaseMapper<SupplierDetail> {
    List<SupplierDetail> selectAllList1();

    List<SupplierDetail> SupplierDetailMapper(Integer supplierId);

    SupplierDetail selectSupplierDetailByDetailId(Integer valueOf);
}
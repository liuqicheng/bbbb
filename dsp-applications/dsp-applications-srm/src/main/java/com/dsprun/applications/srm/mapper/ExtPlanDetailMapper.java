package com.dsprun.applications.srm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dsprun.applications.srm.model.ExtPlanDetail;
import com.dsprun.applications.srm.vo.ExtPlanDetailVO;
import org.apache.ibatis.annotations.Mapper;import java.util.List;

@Mapper
public interface ExtPlanDetailMapper extends BaseMapper<ExtPlanDetail> {
    ExtPlanDetailVO findExtPlanDetailByPlanBatchNo(String planBatchNo);

   // List<ExtPlanDetail> selectDataById(String planBatchNo);

    //ExtPlanDetail findExtPlanDetailByPlanBatchNo(String planBatchNo);
}
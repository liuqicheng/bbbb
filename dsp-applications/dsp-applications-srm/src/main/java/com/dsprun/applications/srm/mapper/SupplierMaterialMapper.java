package com.dsprun.applications.srm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dsprun.applications.srm.model.SupplierMaterial;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SupplierMaterialMapper extends BaseMapper<SupplierMaterial> {

    List<SupplierMaterial> findSupplierMaterialBySupplierId(String supplierId);
}
package com.dsprun.applications.srm.dao;

import java.util.List;
import java.util.Map;

public interface CommonDao {

    String save(String entityName, Map entity);

    int update(String entityName, Map entity);

    boolean deleteById(String entityName, String id);

    List<Map> queryDatas(String entityName);

    List<Map> queryDatas(String entityName, int start, int limit);
}

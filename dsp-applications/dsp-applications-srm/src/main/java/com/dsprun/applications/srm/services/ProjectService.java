package com.dsprun.applications.srm.services;

import com.dsprun.applications.srm.model.Project;
import com.dsprun.applications.srm.model.ProjectAddress;

public interface ProjectService {

    void save(Project projectData, ProjectAddress projectAddr);
}

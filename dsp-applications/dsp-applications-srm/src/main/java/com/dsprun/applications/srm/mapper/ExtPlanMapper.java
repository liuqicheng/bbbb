package com.dsprun.applications.srm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dsprun.applications.srm.model.ExtPlan;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ExtPlanMapper extends BaseMapper<ExtPlan> {
    List<ExtPlan> selectAll();

    ExtPlan selectDataById(String planBatchNo);
}
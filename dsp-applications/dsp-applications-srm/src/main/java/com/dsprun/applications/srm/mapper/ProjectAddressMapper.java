package com.dsprun.applications.srm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dsprun.applications.srm.model.ProjectAddress;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ProjectAddressMapper extends BaseMapper<ProjectAddress> {
    void save(ProjectAddress projectAddr);
}
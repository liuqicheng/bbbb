package com.dsprun.applications.srm.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "supplier_material")
public class SupplierMaterial {
    @TableId(value = "ID", type = IdType.INPUT)
    private String id;

    @TableField(value = "MATERIAL_DIRECTORY_ID")
    private String materialDirectoryId;

    @TableField(value = "MATERIAL_NAME")
    private String materialName;

    @TableField(value = "MATERIAL_CODE")
    private String materialCode;

    @TableField(value = "UNIT")
    private String unit;

    @TableField(value = "SPN_TYP")
    private String spnTyp;

    @TableField(value = "MATERIAL_ID")
    private String materialId;

    @TableField(value = "SUPPLIER_ID")
    private String supplierId;

    @TableField(value = "COMPANY_ID")
    private String companyId;

    public static final String COL_ID = "ID";

    public static final String COL_MATERIAL_DIRECTORY_ID = "MATERIAL_DIRECTORY_ID";

    public static final String COL_MATERIAL_NAME = "MATERIAL_NAME";

    public static final String COL_MATERIAL_CODE = "MATERIAL_CODE";

    public static final String COL_UNIT = "UNIT";

    public static final String COL_SPN_TYP = "SPN_TYP";

    public static final String COL_MATERIAL_ID = "MATERIAL_ID";

    public static final String COL_SUPPLIER_ID = "SUPPLIER_ID";

    public static final String COL_COMPANY_ID = "COMPANY_ID";
}
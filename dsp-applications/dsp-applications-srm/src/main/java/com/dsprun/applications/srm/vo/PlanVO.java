package com.dsprun.applications.srm.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.dsprun.applications.srm.model.PlanDetail;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName(value = "\"PLAN\"")
public class PlanVO {
    /**
     * 计划批次号
     */
    @TableId(value = "plan_batch_no", type = IdType.INPUT)
    private String planBatchNo;

    /**
     * 发起日期
     */
    @TableField(value = "plan_date")
    private Date planDate;

    /**
     * 计划名称
     */
    @TableField(value = "plan_name")
    private String planName;

    /**
     * 计划类型 0 ERP(外部计划)，1SYS (平台计划)
     */
    @TableField(value = "plan_type")
    private BigDecimal planType;

    /**
     * 状态 0:待推送 0：待提交 1：审核中 2 审核完了 3 审核终止
     */
    @TableField(value = "\"status\"")
    private BigDecimal status;

    /**
     * 采购主管 (是不是计划创建人)
     */
    @TableField(value = "plan_holder")
    private String planHolder;

    /**
     * 计划部门名称
     */
    @TableField(value = "plan_org_name")
    private String planOrgName;

    /**
     * 需求公司名称
     */
    @TableField(value = "demand_comp_name")
    private String demandCompName;

    /**
     * 需求公司 ID
     */
    @TableField(value = "demand_comp_id")
    private String demandCompId;

    /**
     * 需求部门
     */
    @TableField(value = "demand_org_name")
    private String demandOrgName;

    /**
     * 需求部门id
     */
    @TableField(value = "demand_org_id")
    private Integer demandOrgId;

    /**
     * 计划员 (是否保留)
     */
    @TableField(value = "applicant")
    private String applicant;

    /**
     * 参与者 (是否保留)
     */
    @TableField(value = "actors")
    private String actors;

    /**
     * 审核任务关联的流程 instance_id
     */
    @TableField(value = "process_instance_id")
    private String processInstanceId;

    /**
     * 添加人
     */
    @TableField(value = "create_user")
    private String createUser;

    /**
     * 添加时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 修改人
     */
    @TableField(value = "update_user")
    private String updateUser;

    /**
     * 修改时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    /**
     * 删除flag0:有效 1：逻辑删除
     */
    @TableField(value = "cancel_flag")
    private BigDecimal cancelFlag;

    /**
     * 备注
     */
    @TableField(value = "comments")
    private String comments;

    private PlanDetail planDetail;

    /**
     * 描述
     */
    @TableField(value = "description")
    private String description;

    public static final String COL_PLAN_BATCH_NO = "plan_batch_no";

    public static final String COL_PLAN_DATE = "plan_date";

    public static final String COL_PLAN_NAME = "plan_name";

    public static final String COL_PLAN_TYPE = "plan_type";

    public static final String COL_STATUS = "status";

    public static final String COL_PLAN_HOLDER = "plan_holder";

    public static final String COL_PLAN_ORG_NAME = "plan_org_name";

    public static final String COL_DEMAND_COMP_NAME = "demand_comp_name";

    public static final String COL_DEMAND_COMP_ID = "demand_comp_id";

    public static final String COL_DEMAND_ORG_NAME = "demand_org_name";

    public static final String COL_DEMAND_ORG_ID = "demand_org_id";

    public static final String COL_APPLICANT = "applicant";

    public static final String COL_ACTORS = "actors";

    public static final String COL_PROCESS_INSTANCE_ID = "process_instance_id";

    public static final String COL_CREATE_USER = "create_user";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_UPDATE_USER = "update_user";

    public static final String COL_UPDATE_TIME = "update_time";

    public static final String COL_CANCEL_FLAG = "cancel_flag";

    public static final String COL_COMMENTS = "comments";

    public static final String COL_DESCRIPTION = "description";

    public static final String PLAN_DETAIL = "planDetail";
    public PlanDetail getPlanDetail() {
        return planDetail;
    }

    public void setPlanDetail(PlanDetail planDetail) {
        this.planDetail = planDetail;
    }
}
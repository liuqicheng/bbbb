package com.dsprun.applications.srm.services;

import com.dsprun.applications.srm.model.ExtPlan;
import com.dsprun.applications.srm.vo.ExtPlanDetailVO;

import java.util.List;

public interface ExternaltaskService {
    List<ExtPlan> selectAll();

    ExtPlanDetailVO findExtPlanById(String planBatchNo);
}

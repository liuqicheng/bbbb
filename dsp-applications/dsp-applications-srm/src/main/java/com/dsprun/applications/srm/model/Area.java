package com.dsprun.applications.srm.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.math.BigDecimal;
import lombok.Data;

@Data
@TableName(value = "area")
public class Area {
    @TableId(value = "id", type = IdType.INPUT)
    private Integer id;

    @TableField(value = "parent_id")
    private BigDecimal parentId;

    @TableField(value = "area_name")
    private String areaName;

    @TableField(value = "is_hot")
    private BigDecimal isHot;

    public static final String COL_ID = "id";

    public static final String COL_PARENT_ID = "parent_id";

    public static final String COL_AREA_NAME = "area_name";

    public static final String COL_IS_HOT = "is_hot";
}
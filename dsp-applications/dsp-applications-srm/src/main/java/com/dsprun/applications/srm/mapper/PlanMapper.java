package com.dsprun.applications.srm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dsprun.applications.srm.model.Plan;
import org.apache.ibatis.annotations.Mapper;import java.util.List;

@Mapper
public interface PlanMapper extends BaseMapper<Plan> {
    List<Plan> selectList();

    List<Plan> selectListByplanBatchNo(String planBatchNo);

    // List<Plan> selectListByplanBatchNo(String planBatchNo);
}
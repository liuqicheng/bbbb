package com.dsprun.applications.srm.services.impl;

import com.dsprun.applications.srm.mapper.SysUseraMapper;
import com.dsprun.applications.srm.model.SysUsera;
import com.dsprun.applications.srm.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
     SysUseraMapper sysUseraMapper;


    @Override
    public List<SysUsera> findUserByName(String buyerName) {

        List<SysUsera> userAll = sysUseraMapper.selectALL(buyerName);
        return null;
    }
}


package com.dsprun.applications.srm.execl;

import com.dsprun.applications.srm.model.PlanDetail;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Service
public class ImportExcelServiceImpl extends ImportExcelBaseService implements IImportExcelService {
    @Override
    public List<PlanDetail> importExcelWithSimple(MultipartFile file,HttpServletRequest req,HttpServletResponse resp) {
        int rowNum = 0;//已取值的行数
        int colNum = 0;//列号
        int realRowCount = 0;//真正有数据的行数

        //得到工作空间
        Workbook workbook = null;
        try {
            workbook = super.getWorkbookByInputStream(file.getInputStream(), file.getOriginalFilename());
        } catch (IOException e) {
            e.printStackTrace();
        }

        //得到工作表
        Sheet sheet = super.getSheetByWorkbook(workbook, 0);
        if (sheet.getRow(2000) != null){
            throw new RuntimeException("系统已限制单批次导入必须小于或等于2000笔！");
        }

        realRowCount = sheet.getPhysicalNumberOfRows();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        List<PlanDetail> list = new ArrayList<>();
        PlanDetail PlanDetail = null;

        for(Row row:sheet) {
            if(realRowCount == rowNum) {
                break;
            }

            if(super.isBlankRow(row)) {//空行跳过
                continue;
            }

            if(row.getRowNum() == -1) {
                continue;
            }else {
                if(row.getRowNum() == 5) {//第一行表头跳过
                    continue;
                }
            }

            rowNum ++;
            colNum = 1;
            PlanDetail = new PlanDetail();

            /*if(rowNum < 5){
                super.validCellValue(sheet, row, ++ colNum, "物料编号");
                PlanDetail.setPlanBatchNo(super.getCellValue(sheet, row, colNum - 1));
            }*/
            super.validCellValue(sheet, row, ++ colNum, "物料编号");
            PlanDetail.setMaterialCode(super.getCellValue(sheet, row, colNum - 1));

            super.validCellValue(sheet, row, ++ colNum, "物料名称");
            PlanDetail.setMaterialName(super.getCellValue(sheet, row, colNum - 1));

            super.validCellValue(sheet, row, ++ colNum, "消费时间");
            try {
                PlanDetail.setArrivalDate(sdf.parse(super.getCellValue(sheet, row, colNum - 1)));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            super.validCellValue(sheet, row, ++ colNum, "消费项目");
            PlanDetail.setSpnTyp(super.getCellValue(sheet, row, colNum - 1));

            list.add(PlanDetail);
        }

        return list;
    }
}


package com.dsprun.applications.srm.controller;

import com.dsprun.applications.srm.model.Plan;
import com.dsprun.applications.srm.model.PlanDetail;
import com.dsprun.applications.srm.services.DailyTaskService;
import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("dailyTask")
public class MyDailyTaskController {

    @Autowired
    DailyTaskService dailyTaskService;
    /**
     * 查看我的日常计划
     * @param request
     * @param
     * @param
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/listView")
    public String listView(HttpServletRequest request, @RequestParam(name = "planBatchNo", defaultValue = "") String planBatchNo,
                             @RequestParam(name = "status", defaultValue = "")String status) {
        System.out.println("获取我的计划分配日常计划信息！");
        //获取当前登入人
       // SysUserPO user = (SysUserPO) request.getSession().getAttribute(SessionUtil.SESSION_PSM_USER);
        ModelMap modelMap = new ModelMap();

            // 查看我的日常计划
          //  PageInfo<Plan> planPage = platformTaskService.selectPageByExample(dataTableJSONRequest,status,user.getACCOUNT());


       // List<Plan>  planList = dailyTaskService.findPlanDetailByPlanBatchNo(planBatchNo);
        //modelMap.put("dailyTaskData",planList);




        return "/srm/index";
    }

}

package com.dsprun.applications.srm.services;

import com.dsprun.applications.srm.model.Plan;
import com.dsprun.applications.srm.model.PlanDetail;
import com.dsprun.applications.srm.vo.PlanVO;
import org.springframework.ui.ModelMap;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface PlatformTaskService {
    List<Plan> selectAllPlatform();

    List<PlanVO> findPlanById(String planBatchNo);

    List<PlanDetail> findPlanDetailByPlanBatchNo(String planBatchNo);

   // List<PlanDetail> importExcelWithSimple(MultipartFile file, HttpServletRequest req, HttpServletResponse resp);

    //ModelMap planExcelImport(MultipartFile , String );
}

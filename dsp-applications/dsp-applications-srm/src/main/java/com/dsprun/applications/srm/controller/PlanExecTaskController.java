package com.dsprun.applications.srm.controller;

import com.alibaba.fastjson.JSON;
import com.dsprun.applications.srm.model.*;
import com.dsprun.applications.srm.services.*;
import com.dsprun.applications.srm.vo.PlanExecVO;
import lombok.Data;
import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("execTask")
public class PlanExecTaskController {


    @Autowired
    private PlanExecTaskService planExecTaskService;

    @Autowired
    private SupplierService supplierService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private UserService userService;

    @Autowired
    private AreaService areaService;
    /**
    *
    *@param获取执行日常计划信息
    **/
    @GetMapping(value = "/listView")
    public String listView(ModelMap map) {
        System.out.println("获取执行日常计划信息！");
        List<PlanExecVO> planExecAll = planExecTaskService.selectExecAll();
        map.addAttribute("planExecAll",planExecAll);
        return "/srm/index";
    }


    @GetMapping(value = "/drupProject/{execId}")
    public String creatProject(ModelMap modelMap,@PathVariable(name = "id") Integer execId){
        System.out.println("跳转到创建项目页面！");
        PlanExecVO planExec = null;
        //判断ID 是否为空
        if(execId!=null && !"".equals(execId)){
            //通过ID获取计划信息
             planExec = planExecTaskService.selectByPrimaryKey(execId);
        }

        if("1".equals(planExec.getBuyer())){
            System.out.println("跳转到创建直接采购项目页面！");

            return "direct/directProject";
        }else if("2".equals(planExec.getBuyer())){
            //查看下一节点审批人
            System.out.println("跳转到创建询价采购项目页面！");
            return "inquiry/inquiryProject";
        }else if("3".equals(planExec.getBuyer())){
            //查看下一节点审批人
            System.out.println("跳转到创建竞价采购项目页面！");
            return "bidding/biddingProject";
        }else if("4".equals(planExec.getBuyer())){
            //查看下一节点审批人
            System.out.println("跳转到创建公开采购项目页面！");
            return "public/publicProject";
        }else if("5".equals(planExec.getBuyer())){
            //查看下一节点审批人
            System.out.println("跳转到创建邀请采购项目页面！");
            return "invitation/invitationProject";
        }
        return "";
    }


    /**
     * 项目创建提交
     */
    //@PathVariable(name ="execId") Integer execId, String proname, Integer buy_type, String plan_batch_names,String settlement_method, Data publish_date,String pur_conditionString String description
    @GetMapping(value = "/creatProject/{execId}")
    public void projectSubmit(ModelMap modelMap,@PathVariable(name ="execId") Integer execId,@RequestParam(name = "projectData", defaultValue = "") Project projectData,@RequestParam(name = "projectAddr", defaultValue = "") ProjectAddress projectAddr){
        System.out.println("创建项目页提交！");
        projectService.save(projectData,projectAddr);
    }



    /**
     * 项目合并
     */
    @GetMapping(value = "/creatProject1/{execId}")
    public String packItem(@RequestParam(name = "ids", defaultValue = "") String ids) {
        System.out.println("合并计划！");
        String[] idss = ids.split("，");
        List<String> planExeIdList = new ArrayList<String>();
        for (String id : idss) {
            if (id != null) {
                planExeIdList.add(id);
            }
        }
        return "";
    }





















    /**
     * 获取所有采购员
     */
    @ResponseBody
    @GetMapping(value = "getAllBuyer")
    public String getAllBuyer(@RequestParam(name = "aoData", defaultValue = "") String aoData,@RequestParam(name = "buyerName", defaultValue = "") String buyerName,@RequestParam(name = "planIds", defaultValue = "") String planIds){

        ModelMap modelMap = new ModelMap();
        String newPlanIds = planIds.replace("，", ",");

       // List<SysUsera> findUserByName = planExecTaskService.findbuyerName(buyerName);
            List<SysUsera> findUserByName = userService.findUserByName(buyerName);
            modelMap.put("userAll", findUserByName);

        System.out.println("获取所有采购员!");
        return "获取所有采购员";
    }


    /**
     * 转移采购员
     */
    @ResponseBody
    @RequestMapping(value = "toOther", method = { RequestMethod.GET, RequestMethod.POST })
    public String toOther(@RequestParam(name = "planExecIds", defaultValue = "") String planExecIds,HttpServletRequest request,
                            @RequestParam(name = "buyerId", defaultValue = "") String buyerId){
        ModelMap modelMap = new ModelMap();
        Date date  = new Date();
        //获取当前登入人
        //SysUserPO user = (SysUserPO) request.getSession().getAttribute(SessionUtil.SESSION_PSM_USER);

            String[] planExecId = planExecIds.split("，");
            for (int i = 0; i < planExecId.length; i++) {
                String planExecid = String.valueOf(planExecId[i]);
                PlanExec planExec = planExecTaskService.findPlanExecById(Integer.parseInt(planExecid));

                /************判断是否有采购权限********开始******/
                List<PlanExecDetail> planExecDetailList = planExecTaskService.findPlanExecDetailList(planExec.getId());
            }
                //************判断是否有采购权限********结束******//*
                return null;

    }

}

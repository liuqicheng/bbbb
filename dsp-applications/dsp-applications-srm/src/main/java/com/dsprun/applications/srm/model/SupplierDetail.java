package com.dsprun.applications.srm.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "supplier_detail")
public class SupplierDetail {
    /*public static final String COL_供应商ID = "供应商id";
    public static final String COL_供应商编码 = "供应商编码";
    public static final String COL_公司名字 = "公司名字";
    public static final String COL_企业级别 = "企业级别";
    public static final String COL_组织机构代码 = "组织机构代码";
    public static final String COL_T税务登记号AX_NO = "t税务登记号ax_no";
    public static final String COL_营业执照号 = "营业执照号";
    public static final String COL_注册资金 = "注册资金";
    public static final String COL_成立日期 = "成立日期";
    public static final String COL_开户名称 = "开户名称";
    public static final String COL_开户银行 = "开户银行";
    public static final String COL_银行账号 = "银行账号";
    public static final String COL_供应商性质 = "供应商性质";
    public static final String COL_企业性质 = "企业性质";
    public static final String COL_进出口权0 = "进出口权0";
    public static final String COL_法人代表 = "法人代表";
    public static final String COL_法人身份证 = "法人身份证";
    public static final String COL_业务联系人 = "业务联系人";
    public static final String COL_联系手机 = "联系手机";
    public static final String COL_联系座机 = "联系座机";
    public static final String COL_分机号 = "分机号";
    public static final String COL_联系人邮箱 = "联系人邮箱";
    public static final String COL_企业官网 = "企业官网";
    public static final String COL_主要业绩 = "主要业绩";
    public static final String COL_供应商类别 = "供应商类别";
    public static final String COL_验审费标准ID = "验审费标准id";
    public static final String COL_添加人 = "添加人";
    public static final String COL_添加时间 = "添加时间";
    public static final String COL_修改人 = "修改人";
    public static final String COL_修改时间 = "修改时间";
    public static final String COL_删除FLAG0 = "删除flag0";
    public static final String COL_备考 = "备考";
    public static final String COL_注册地址 = "注册地址";
    public static final String COL_经营产品 = "经营产品";
    public static final String COL_所选采购商组织编码 = "所选采购商组织编码";*/
    /**
     * id自增长
     */
    @TableId(value = "id", type = IdType.INPUT)
    private Integer id;

    /**
     * 供应商id
     */
    @TableField(value = "supplier_id")
    private BigDecimal supplierId;

    @TableField(value = "supplier_no")
    private String supplierNo;

    @TableField(value = "company_name")
    private String companyName;

    @TableField(value = "supplier_level")
    private String supplierLevel;

    @TableField(value = "org_code")
    private String orgCode;

    @TableField(value = "tax_no")
    private String taxNo;

    @TableField(value = "business_no")
    private String businessNo;

    /**
     * 注册资金
     */
    @TableField(value = "registered_capital")
    private BigDecimal registeredCapital;

    @TableField(value = "found_date")
    private Date foundDate;

    @TableField(value = "account_name")
    private String accountName;

    @TableField(value = "account_bank")
    private String accountBank;

    @TableField(value = "account_no")
    private String accountNo;

    @TableField(value = "supplier_nature")
    private String supplierNature;

    @TableField(value = "company_nature")
    private String companyNature;

    /**
     * 进出口权0:无 1：有
     */
    @TableField(value = "imp_exp_auth")
    private BigDecimal impExpAuth;

    @TableField(value = "legal_name")
    private String legalName;

    @TableField(value = "legal_id_card")
    private String legalIdCard;

    @TableField(value = "contacts")
    private String contacts;

    @TableField(value = "contact_phone")
    private String contactPhone;

    @TableField(value = "contact_tel")
    private String contactTel;

    @TableField(value = "contact_tel_extension")
    private String contactTelExtension;

    @TableField(value = "contacts_mail")
    private String contactsMail;

    @TableField(value = "company_website")
    private String companyWebsite;

    @TableField(value = "main_performance")
    private String mainPerformance;

    /**
     * 供应商类别
     */
    @TableField(value = "supplier_category_code")
    private BigDecimal supplierCategoryCode;

    /**
     * 验审费标准id
     */
    @TableField(value = "annual_fee_id")
    private BigDecimal annualFeeId;

    @TableField(value = "insert_user")
    private String insertUser;

    @TableField(value = "insert_time")
    private Date insertTime;

    @TableField(value = "update_user")
    private String updateUser;

    @TableField(value = "update_time")
    private Date updateTime;

    /**
     * 删除flag0:有效 1：逻辑删除
     */
    @TableField(value = "cancel_flag")
    private BigDecimal cancelFlag;

    @TableField(value = "comments")
    private String comments;

    @TableField(value = "registered_address")
    private String registeredAddress;

    @TableField(value = "products")
    private String products;

    @TableField(value = "purchasers")
    private String purchasers;

    public static final String COL_ID = "id";

    public static final String COL_SUPPLIER_ID = "supplier_id";

    public static final String COL_SUPPLIER_NO = "supplier_no";

    public static final String COL_COMPANY_NAME = "company_name";

    public static final String COL_SUPPLIER_LEVEL = "supplier_level";

    public static final String COL_ORG_CODE = "org_code";

    public static final String COL_TAX_NO = "tax_no";

    public static final String COL_BUSINESS_NO = "business_no";

    public static final String COL_REGISTERED_CAPITAL = "registered_capital";

    public static final String COL_FOUND_DATE = "found_date";

    public static final String COL_ACCOUNT_NAME = "account_name";

    public static final String COL_ACCOUNT_BANK = "account_bank";

    public static final String COL_ACCOUNT_NO = "account_no";

    public static final String COL_SUPPLIER_NATURE = "supplier_nature";

    public static final String COL_COMPANY_NATURE = "company_nature";

    public static final String COL_IMP_EXP_AUTH = "imp_exp_auth";

    public static final String COL_LEGAL_NAME = "legal_name";

    public static final String COL_LEGAL_ID_CARD = "legal_id_card";

    public static final String COL_CONTACTS = "contacts";

    public static final String COL_CONTACT_PHONE = "contact_phone";

    public static final String COL_CONTACT_TEL = "contact_tel";

    public static final String COL_CONTACT_TEL_EXTENSION = "contact_tel_extension";

    public static final String COL_CONTACTS_MAIL = "contacts_mail";

    public static final String COL_COMPANY_WEBSITE = "company_website";

    public static final String COL_MAIN_PERFORMANCE = "main_performance";

    public static final String COL_SUPPLIER_CATEGORY_CODE = "supplier_category_code";

    public static final String COL_ANNUAL_FEE_ID = "annual_fee_id";

    public static final String COL_INSERT_USER = "insert_user";

    public static final String COL_INSERT_TIME = "insert_time";

    public static final String COL_UPDATE_USER = "update_user";

    public static final String COL_UPDATE_TIME = "update_time";

    public static final String COL_CANCEL_FLAG = "cancel_flag";

    public static final String COL_COMMENTS = "comments";

    public static final String COL_REGISTERED_ADDRESS = "registered_address";

    public static final String COL_PRODUCTS = "products";

    public static final String COL_PURCHASERS = "purchasers";
}
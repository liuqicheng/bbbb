package com.dsprun.applications.srm.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
@TableName(value = "plan_exec")
public class PlanExec {
    /**
     * id自增长
     */
    @TableId(value = "id", type = IdType.INPUT)
    private Integer id;

    /**
     * 计划批次号 (多个逗号分隔)
     */
    @TableField(value = "plan_batch_nos")
    private String planBatchNos;

    /**
     * 计划名称 (多个逗号分隔)
     */
    @TableField(value = "plan_batch_names")
    private String planBatchNames;

    /**
     * 计划区分0：erp 1：sys
     */
    @TableField(value = "plan_type")
    private BigDecimal planType;

    /**
     * 合并标识0:无 1：已合并 2:合并后
     */
    @TableField(value = "union_flag")
    private BigDecimal unionFlag;

    /**
     * 合并数据来源
     */
    @TableField(value = "union_from")
    private String unionFrom;

    /**
     * 需求公司名称
     */
    @TableField(value = "demand_comp_name")
    private String demandCompName;

    /**
     * 需求公司 ID
     */
    @TableField(value = "demand_comp_id")
    private String demandCompId;

    /**
     * 需求部门名称 (多个逗号分隔)
     */
    @TableField(value = "demand_org_names")
    private String demandOrgNames;

    /**
     * 需求部门 ID (多个逗号分隔)
     */
    @TableField(value = "demand_org_ids")
    private String demandOrgIds;

    /**
     * 状态0：待发起 1：已发起
     */
    @TableField(value = "\"status\"")
    private BigDecimal status;

    /**
     * 采购形式1 直接采购 2 询价采购 3 竞价采购 4 公开招标 5邀请招标 6 框架协议
     */
    @TableField(value = "buy_type")
    private BigDecimal buyType;

    /**
     * 采购人
     */
    @TableField(value = "buyer")
    private String buyer;

    /**
     * 创建人
     */
    @TableField(value = "create_user")
    private String createUser;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 修改人
     */
    @TableField(value = "update_user")
    private String updateUser;

    /**
     * 修改时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    /**
     * 删除flag0:有效 1：逻辑删除
     */
    @TableField(value = "cancel_flag")
    private BigDecimal cancelFlag;

    /**
     * 备注
     */
    @TableField(value = "comments")
    private String comments;

    public static final String COL_ID = "id";

    public static final String COL_PLAN_BATCH_NOS = "plan_batch_nos";

    public static final String COL_PLAN_BATCH_NAMES = "plan_batch_names";

    public static final String COL_PLAN_TYPE = "plan_type";

    public static final String COL_UNION_FLAG = "union_flag";

    public static final String COL_UNION_FROM = "union_from";

    public static final String COL_DEMAND_COMP_NAME = "demand_comp_name";

    public static final String COL_DEMAND_COMP_ID = "demand_comp_id";

    public static final String COL_DEMAND_ORG_NAMES = "demand_org_names";

    public static final String COL_DEMAND_ORG_IDS = "demand_org_ids";

    public static final String COL_STATUS = "status";

    public static final String COL_BUY_TYPE = "buy_type";

    public static final String COL_BUYER = "buyer";

    public static final String COL_CREATE_USER = "create_user";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_UPDATE_USER = "update_user";

    public static final String COL_UPDATE_TIME = "update_time";

    public static final String COL_CANCEL_FLAG = "cancel_flag";

    public static final String COL_COMMENTS = "comments";

}
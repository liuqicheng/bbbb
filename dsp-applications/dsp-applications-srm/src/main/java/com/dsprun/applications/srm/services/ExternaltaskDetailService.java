package com.dsprun.applications.srm.services;

import com.dsprun.applications.srm.model.ExtPlanDetail;

import java.util.List;

public interface ExternaltaskDetailService {

    List<ExtPlanDetail> findExtPlanDetailByPlanBatchNo(String planBatchNo);
}

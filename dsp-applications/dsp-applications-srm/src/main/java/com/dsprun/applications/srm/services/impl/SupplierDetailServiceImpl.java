package com.dsprun.applications.srm.services.impl;

import com.dsprun.applications.srm.mapper.SupplierDetailMapper;
import com.dsprun.applications.srm.model.SupplierDetail;
import com.dsprun.applications.srm.services.SupplierDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SupplierDetailServiceImpl implements SupplierDetailService {

    @Autowired
    private SupplierDetailMapper supplierDetailMapper;
    @Override
    public List<SupplierDetail> selectSupplierInfoBySupplierId(Integer supplierId) {

        List<SupplierDetail> supplierDetailList =  supplierDetailMapper.SupplierDetailMapper(supplierId);
        return supplierDetailList;
    }


    @Override
    public List<SupplierDetail> selectAll() {
        List<SupplierDetail> selectAllDtata = supplierDetailMapper.selectAllList1();
        return selectAllDtata;
    }

    @Override
    public SupplierDetail findSupplierDetailBySupplierDetailId(Integer valueOf) {

        SupplierDetail SupplierDetail = supplierDetailMapper.selectSupplierDetailByDetailId(valueOf);
        return SupplierDetail;
    }

}

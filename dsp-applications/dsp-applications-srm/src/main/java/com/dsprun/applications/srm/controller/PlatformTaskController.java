package com.dsprun.applications.srm.controller;

import com.dsprun.applications.srm.model.Plan;
import com.dsprun.applications.srm.model.PlanDetail;
import com.dsprun.applications.srm.services.PlatformTaskService;
import com.dsprun.applications.srm.vo.PlanVO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("platformTask")
public class PlatformTaskController {

    @Resource
    PlatformTaskService platformTaskService;


    /*
     * 导入
     *
     *
     *
     * */

    @ResponseBody
    @RequestMapping(value = "planExcelImport", method = { RequestMethod.GET, RequestMethod.POST })
    public ModelMap planExcelImport(@RequestParam("planExcel") MultipartFile planExcel, @RequestParam(name = "planBatchNo", defaultValue = "") String planBatchNo) {
        ModelMap modelMap = new ModelMap();
        try {
           // modelMap = platformTaskService.planExcelImport(planExcel,planBatchNo);
        } catch (Exception e) {
           /* log.debug("e={}", e);
            modelMap.put("status", FAILURE);
            modelMap.put("msg", "导入数据失败");*/
        }
        return modelMap;
    }





















    @ResponseBody
    @GetMapping(value = "/listView")
    public String listView(ModelMap modelMap,HttpServletRequest request) {
        System.out.println("获取平台计划信息！");
            // 查看全部平台计划
            List<Plan> planFormData = platformTaskService.selectAllPlatform();
            modelMap.put("planForm",planFormData);
           // modelMap.put("status", "FAILURE");
        return "to-plateformPage";
    }
        /*
        * 点击更多
        *
        *
        * */
    @RequestMapping(value = "/viewItem/{planBatchNo}")
    public String viewItem(ModelMap modelMap, HttpServletRequest request, @RequestParam(name = "planBatchNo", defaultValue = "")String planBatchNo){
        System.out.println("跳转平台计划详情！");
        if(planBatchNo != null && !"".equals(planBatchNo)){
            List<PlanVO> plan = platformTaskService.findPlanById(planBatchNo);
           // List<PlanDetail> planDetailList = platformTaskService.findPlanDetailByPlanBatchNo(planBatchNo);
            modelMap.put("plan", plan);
            //modelMap.put("planDetailList", planDetailList);
        }
        return "platformtaskDetail";
    }



}

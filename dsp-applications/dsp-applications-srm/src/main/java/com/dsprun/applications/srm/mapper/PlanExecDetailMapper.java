package com.dsprun.applications.srm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dsprun.applications.srm.model.PlanExecDetail;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;
import java.util.List;

@Mapper
public interface PlanExecDetailMapper extends BaseMapper<PlanExecDetail> {
    List<PlanExecDetail> findPlanExecDetailById(String planExecId);

    PlanExecDetail getRePlanExecDetailVOListByPrimaryKey(Integer id);

    List<PlanExecDetail> findPlanExecDetailByExecId(Integer id);
}
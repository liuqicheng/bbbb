package com.dsprun.applications.srm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dsprun.applications.srm.model.Plan;
import com.dsprun.applications.srm.model.PlanDetail;
import com.dsprun.applications.srm.vo.PlanVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PlanDetailMapper extends BaseMapper<PlanDetail> {
    PlanVO findPlanDetailByPlanBatchNo(String planBatchNo);


    // Plan findPlanDetailByPlanBatchNo(String planBatchNo);
}
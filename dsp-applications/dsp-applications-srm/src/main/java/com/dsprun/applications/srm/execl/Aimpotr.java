package com.dsprun.applications.srm.execl;

import com.dsprun.applications.srm.excl.BusinessException;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Aimpotr {
    private final static String excel2003L = ".xls"; // 2003- 版本的excel
    private final static String excel2007U = ".xlsx"; // 2007+ 版本的excel

    /**
     * 描述：获取IO流中的数据，组装成List<List<Object>>对象
     *
     * @param in,fileName
     * @return
     * @throws IOException
     */
    public static List<List<Object>> getBankListByExcel(InputStream in, String fileName, int startRow, int startCol) throws Exception {
        List<List<Object>> list = null;

        // 创建Excel工作薄
        Workbook work = getWorkbook(in, fileName);
        if (null == work) {
            throw new Exception("创建Excel工作薄为空！");
        }
        Sheet sheet = null;
        Row row = null;
        Cell cell = null;

        list = new ArrayList<List<Object>>();
        // 遍历Excel中所有的sheet
        for (int i = 0; i < work.getNumberOfSheets(); i++) {
            sheet = work.getSheetAt(i);
            if("hidden_company".equals(sheet.getSheetName())){
                continue;
            }
            if (sheet == null) {
                continue;
            }

            // 遍历当前sheet中的所有行
            if (sheet.getFirstRowNum() > startRow) {
                throw new BusinessException("MSG1003W", new Throwable("导入文件的开始行不正确"));
            }

            for (int j = startRow; j <= sheet.getLastRowNum(); j++) {
                row = sheet.getRow(j);
                if (row == null) {
                    continue;
                }

                //判断第一列是否为空，如果为空跳过该行数据的导入。
                Cell firstCell = row.getCell(0);
                if(firstCell == null){
                    continue;
                }

                if (row.getFirstCellNum() > startCol) {
                    throw new BusinessException("MSG1003W", new Throwable("导入文件的第" + row + "行的开始列不正确"));
                }

                // 遍历所有的列
                List<Object> li = new ArrayList<Object>();
                for (int y = row.getFirstCellNum(); y < row.getLastCellNum(); y++) {
                    cell = row.getCell(y);
                    if(cell==null){
                        li.add("");
                    }else{
                        li.add(getCellValue(cell));
                    }
                }
                list.add(li);
            }
        }
        work.close();
        return list;
    }

    /**
     * 描述：根据文件后缀，自适应上传文件的版本
     *
     * @param inStr,fileName
     * @return
     * @throws Exception
     */
    public static Workbook getWorkbook(InputStream inStr, String fileName) throws Exception {
        Workbook wb = null;
        String fileType = fileName.substring(fileName.lastIndexOf("."));
        if (excel2003L.equals(fileType)) {
            wb = new HSSFWorkbook(inStr); // 2003-
        } else if (excel2007U.equals(fileType)) {
            wb = new XSSFWorkbook(inStr); // 2007+
        } else {
            throw new BusinessException("MSG1003W", new Throwable("导入文件的文件格式错误"));
        }
        return wb;
    }

    /**
     * 描述：对表格中数值进行格式化
     *
     * @param cell
     * @return
     */
    @SuppressWarnings("deprecation")
    public static Object getCellValue(Cell cell) {
        Object value = null;
        DecimalFormat df = new DecimalFormat("#.##"); // 格式化number String字符
        FastDateFormat fdf = getDatePattern(); // 日期格式化
        DecimalFormat df2 = getBigDecimalFormat();// 格式化数字

        switch (cell.getCellType()) {
            case STRING:
                value = cell.getRichStringCellValue().getString();
                break;
            case NUMERIC:
                if ("General".equals(cell.getCellStyle().getDataFormatString())) {
                    value = df.format(cell.getNumericCellValue());
                } else if ("m/d/yy".equals(cell.getCellStyle().getDataFormatString())) {
                    value = fdf.format(cell.getDateCellValue());
                } else {
                    value = df2.format(cell.getNumericCellValue());
                }
                break;
            case BOOLEAN:
                value = cell.getBooleanCellValue();
                break;
            case BLANK:
                value = "";
                break;
            default:
                break;
        }
        return value;
    }

    protected static FastDateFormat getDatePattern() {
        return DateFormatUtils.ISO_DATE_FORMAT;
    }

    protected static DecimalFormat getBigDecimalFormat() {
        return new DecimalFormat("#.##");
    }

/*
    public static List<Object> excelToBeanConvert(String outClassName, List<List<Object>> excelObjList, List<ColumInfo> headers, int startRow) throws Exception {

        List<Object> outObjList = new ArrayList<>();
        boolean existError = false;
        StringBuffer errorMessage = new StringBuffer();
        for (int i = 0; i < excelObjList.size(); i++) {
            Object outObj = null;
            try {
                outObj = Class.forName(outClassName).newInstance();
            } catch (Exception e) {
                System.out.println("异常MSG1003W");
            }

            List<Object> excelObj = excelObjList.get(i);



            for (int j = 0; j < headers.size(); j++) {
                Method m;
                Class<?> fileType = null;
                try {
                    fileType = outObj.getClass().getDeclaredField(headers.get(j).getColum()).getType();
                    m = outObj.getClass().getDeclaredMethod(headers.get(j).getSetMethod(), fileType);
                    if ((j >= excelObj.size() || excelObj.get(j) == null || "".equals(excelObj.get(j))) && (headers.get(j).isNullEnable())) {
                        // System.out.println(" is null type is:" + fileType);
                        Object invokeObj = convertObj((Object)"", fileType);
                        m.invoke(outObj, invokeObj);
                        continue;
                    }
                    Object invokeObj = convertObj(excelObj.get(j), fileType);
                    m.invoke(outObj, invokeObj);
                } catch (Exception e) {
                    existError = true;
                    errorMessage.append("导入文件的第");
                    errorMessage.append(i + 1 + startRow);
                    errorMessage.append("行的");
                    errorMessage.append(j + 1);
                    errorMessage.append("列的内容有错误。\n");
                }
            }
            outObjList.add(outObj);
        }
        if (existError) {
            throw new BusinessException("MSG1003W", new Throwable(errorMessage.toString()));
        }
        return outObjList;
    }
*/

    private static Object convertObj(Object obj, Class<?> fileType) {
        String type = fileType.getSimpleName();
        if ("String".equals(type)) {
            return (String) obj;
        } else if ("Integer".equals(type)) {
            return Integer.valueOf((String) obj);
        } else if ("int".equals(type)) {
            return Integer.valueOf((String) obj);
        } else if ("Long".equals(type)) {
            return Long.valueOf((String) obj);
        } else if ("long".equals(type)) {
            return Long.valueOf((String) obj);
        } else if ("BigDecimal".equals(type)) {
            return (new BigDecimal((String) obj)).setScale(getBigDecimalScal(), BigDecimal.ROUND_HALF_UP);
            // throw new NumberFormatException();
        } else if ("Date".equals(type)) {
            // Date date = new Date((String)obj);
            //return DateUtil.convertStrToDate((String) obj, getDatePattern().getPattern());
            return "11111111111";
        } else {
            return obj.toString();
        }
    }

    protected static int getBigDecimalScal() {
        return 2;
    }
}

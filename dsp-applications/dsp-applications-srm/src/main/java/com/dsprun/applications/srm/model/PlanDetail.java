package com.dsprun.applications.srm.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName(value = "plan_detail")
public class PlanDetail {
    /**
     * 计划批次号
     */
    @TableId(value = "plan_batch_no", type = IdType.INPUT)
    private String planBatchNo;

    /**
     * 物料编码
     */
   // @TableId(value = "material_code", type = IdType.INPUT)
    @TableField(value = "material_code")
    private String materialCode;

    /**
     * 物料名称
     */
    @TableField(value = "material_name")
    private String materialName;

    /**
     * 规格型号
     */
    @TableField(value = "spn_typ")
    private String spnTyp;

    /**
     * 计量单位
     */
    @TableField(value = "unit")
    private String unit;

    /**
     * 数量
     */
    @TableField(value = "amount")
    private BigDecimal amount;

    /**
     * 制造厂商 
     */
    @TableField(value = "manufacturer")
    private String manufacturer;

    /**
     * 到货日期
     */
    @TableField(value = "arrival_date")
    private Date arrivalDate;

    /**
     * 采购周期
     */
    @TableField(value = "pur_cycle")
    private String purCycle;

    /**
     * 采购合同号
     */
    @TableField(value = "sale_contract_no")
    private String saleContractNo;

    /**
     * 单包装数量
     */
    @TableField(value = "package_amount")
    private BigDecimal packageAmount;

    /**
     * 项目名称
     */
    @TableField(value = "pro_name")
    private String proName;

    /**
     * 生产订单号
     */
    @TableField(value = "po_num")
    private String poNum;

    /**
     * 采购形式
     */
    @TableField(value = "buy_type")
    private BigDecimal buyType;

    /**
     * 采购人
     */
    @TableField(value = "buyer")
    private String buyer;

    /**
     * 添加人
     */
    @TableField(value = "create_user")
    private String createUser;

    /**
     * 添加时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 修改人
     */
    @TableField(value = "update_user")
    private String updateUser;

    /**
     * 修改时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    /**
     * 删除flag0:有效 1：逻辑删除
     */
    @TableField(value = "cancel_flag")
    private BigDecimal cancelFlag;

    /**
     * 备注
     */
    @TableField(value = "comments")
    private String comments;

    public static final String COL_PLAN_BATCH_NO = "plan_batch_no";

    public static final String COL_MATERIAL_CODE = "material_code";

    public static final String COL_MATERIAL_NAME = "material_name";

    public static final String COL_SPN_TYP = "spn_typ";

    public static final String COL_UNIT = "unit";

    public static final String COL_AMOUNT = "amount";

    public static final String COL_MANUFACTURER = "manufacturer";

    public static final String COL_ARRIVAL_DATE = "arrival_date";

    public static final String COL_PUR_CYCLE = "pur_cycle";

    public static final String COL_SALE_CONTRACT_NO = "sale_contract_no";

    public static final String COL_PACKAGE_AMOUNT = "package_amount";

    public static final String COL_PRO_NAME = "pro_name";

    public static final String COL_PO_NUM = "po_num";

    public static final String COL_BUY_TYPE = "buy_type";

    public static final String COL_BUYER = "buyer";

    public static final String COL_CREATE_USER = "create_user";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_UPDATE_USER = "update_user";

    public static final String COL_UPDATE_TIME = "update_time";

    public static final String COL_CANCEL_FLAG = "cancel_flag";

    public static final String COL_COMMENTS = "comments";

    /**
     * 获取计划批次号
     *
     * @return plan_batch_no - 计划批次号
     */
    public String getPlanBatchNo() {
        return planBatchNo;
    }

    /**
     * 设置计划批次号
     *
     * @param planBatchNo 计划批次号
     */
    public void setPlanBatchNo(String planBatchNo) {
        this.planBatchNo = planBatchNo;
    }

    /**
     * 获取物料编码
     *
     * @return material_code - 物料编码
     */
    public String getMaterialCode() {
        return materialCode;
    }

    /**
     * 设置物料编码
     *
     * @param materialCode 物料编码
     */
    public void setMaterialCode(String materialCode) {
        this.materialCode = materialCode;
    }

    /**
     * 获取物料名称
     *
     * @return material_name - 物料名称
     */
    public String getMaterialName() {
        return materialName;
    }

    /**
     * 设置物料名称
     *
     * @param materialName 物料名称
     */
    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    /**
     * 获取规格型号
     *
     * @return spn_typ - 规格型号
     */
    public String getSpnTyp() {
        return spnTyp;
    }

    /**
     * 设置规格型号
     *
     * @param spnTyp 规格型号
     */
    public void setSpnTyp(String spnTyp) {
        this.spnTyp = spnTyp;
    }

    /**
     * 获取计量单位
     *
     * @return unit - 计量单位
     */
    public String getUnit() {
        return unit;
    }

    /**
     * 设置计量单位
     *
     * @param unit 计量单位
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }

    /**
     * 获取数量
     *
     * @return amount - 数量
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * 设置数量
     *
     * @param amount 数量
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * 获取制造厂商 
     *
     * @return manufacturer - 制造厂商 
     */
    public String getManufacturer() {
        return manufacturer;
    }

    /**
     * 设置制造厂商 
     *
     * @param manufacturer 制造厂商 
     */
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    /**
     * 获取到货日期
     *
     * @return arrival_date - 到货日期
     */
    public Date getArrivalDate() {
        return arrivalDate;
    }

    /**
     * 设置到货日期
     *
     * @param arrivalDate 到货日期
     */
    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    /**
     * 获取采购周期
     *
     * @return pur_cycle - 采购周期
     */
    public String getPurCycle() {
        return purCycle;
    }

    /**
     * 设置采购周期
     *
     * @param purCycle 采购周期
     */
    public void setPurCycle(String purCycle) {
        this.purCycle = purCycle;
    }

    /**
     * 获取采购合同号
     *
     * @return sale_contract_no - 采购合同号
     */
    public String getSaleContractNo() {
        return saleContractNo;
    }

    /**
     * 设置采购合同号
     *
     * @param saleContractNo 采购合同号
     */
    public void setSaleContractNo(String saleContractNo) {
        this.saleContractNo = saleContractNo;
    }

    /**
     * 获取单包装数量
     *
     * @return package_amount - 单包装数量
     */
    public BigDecimal getPackageAmount() {
        return packageAmount;
    }

    /**
     * 设置单包装数量
     *
     * @param packageAmount 单包装数量
     */
    public void setPackageAmount(BigDecimal packageAmount) {
        this.packageAmount = packageAmount;
    }

    /**
     * 获取项目名称
     *
     * @return pro_name - 项目名称
     */
    public String getProName() {
        return proName;
    }

    /**
     * 设置项目名称
     *
     * @param proName 项目名称
     */
    public void setProName(String proName) {
        this.proName = proName;
    }

    /**
     * 获取生产订单号
     *
     * @return po_num - 生产订单号
     */
    public String getPoNum() {
        return poNum;
    }

    /**
     * 设置生产订单号
     *
     * @param poNum 生产订单号
     */
    public void setPoNum(String poNum) {
        this.poNum = poNum;
    }

    /**
     * 获取采购形式
     *
     * @return buy_type - 采购形式
     */
    public BigDecimal getBuyType() {
        return buyType;
    }

    /**
     * 设置采购形式
     *
     * @param buyType 采购形式
     */
    public void setBuyType(BigDecimal buyType) {
        this.buyType = buyType;
    }

    /**
     * 获取采购人
     *
     * @return buyer - 采购人
     */
    public String getBuyer() {
        return buyer;
    }

    /**
     * 设置采购人
     *
     * @param buyer 采购人
     */
    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    /**
     * 获取添加人
     *
     * @return create_user - 添加人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置添加人
     *
     * @param createUser 添加人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * 获取添加时间
     *
     * @return create_time - 添加时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置添加时间
     *
     * @param createTime 添加时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取修改人
     *
     * @return update_user - 修改人
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置修改人
     *
     * @param updateUser 修改人
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    /**
     * 获取修改时间
     *
     * @return update_time - 修改时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置修改时间
     *
     * @param updateTime 修改时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取删除flag0:有效 1：逻辑删除
     *
     * @return cancel_flag - 删除flag0:有效 1：逻辑删除
     */
    public BigDecimal getCancelFlag() {
        return cancelFlag;
    }

    /**
     * 设置删除flag0:有效 1：逻辑删除
     *
     * @param cancelFlag 删除flag0:有效 1：逻辑删除
     */
    public void setCancelFlag(BigDecimal cancelFlag) {
        this.cancelFlag = cancelFlag;
    }

    /**
     * 获取备注
     *
     * @return comments - 备注
     */
    public String getComments() {
        return comments;
    }

    /**
     * 设置备注
     *
     * @param comments 备注
     */
    public void setComments(String comments) {
        this.comments = comments;
    }
}
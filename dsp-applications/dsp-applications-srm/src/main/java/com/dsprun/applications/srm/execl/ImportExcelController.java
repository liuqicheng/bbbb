package com.dsprun.applications.srm.execl;

import com.dsprun.applications.srm.model.PlanDetail;
import com.dsprun.applications.srm.services.PlatformTaskService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping("up")
public class ImportExcelController {


    @Resource
    IImportExcelService iImportExcelService;

    @GetMapping(value="/withSimple1")
    @ResponseBody
    public String withSimple1(MultipartFile file, HttpServletRequest req, HttpServletResponse resp) {
        return "aaa";
    }


    @PostMapping(value="/withSimple")
    public String withSimple(@RequestParam("upload") MultipartFile file, HttpServletRequest req, HttpServletResponse resp) {
        List<PlanDetail> list = iImportExcelService.importExcelWithSimple(file, req, resp);

        if(list == null || list.size() == 0 ) {
            return "fail";
        }

        for(PlanDetail bean:list) {
            System.out.println(bean.toString());
        }

        //批量插入list到数据库

        return "success";
    }
}

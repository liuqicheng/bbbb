package com.dsprun.applications.srm.controller;

import com.dsprun.applications.srm.mapper.PurchasingOrganizationMapper;
import com.dsprun.applications.srm.model.PurchasingOrganization;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author Zhao Jun
 * 2020/11/1 19:54
 */
@RestController
@RequestMapping("/purchasing")
public class PurchasingOrganizationController {

    @Resource
    private PurchasingOrganizationMapper purchasingOrganizationMapper;

    @GetMapping("/get/{id}")
    public PurchasingOrganization get(@PathVariable Integer id) {
        return purchasingOrganizationMapper.selectById(id);
    }

    @GetMapping("/save")
    public void save(PurchasingOrganization purchasingOrganization) {
        purchasingOrganizationMapper.insert(purchasingOrganization);
    }

}
package com.dsprun.applications.srm.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "sys_usera")
public class SysUsera {
    @TableId(value = "USER_ID", type = IdType.INPUT)
    private String userId;

    @TableField(value = "CODE")
    private String code;

    @TableField(value = "\"NAME\"")
    private String name;

    @TableField(value = "ACCOUNT")
    private String account;

    @TableField(value = "\"PASSWORD\"")
    private String password;

    @TableField(value = "EMAIL")
    private String email;

    @TableField(value = "MOBILE")
    private String mobile;

    @TableField(value = "TELEPHONE")
    private String telephone;

    public static final String COL_USER_ID = "USER_ID";

    public static final String COL_CODE = "CODE";

    public static final String COL_NAME = "NAME";

    public static final String COL_ACCOUNT = "ACCOUNT";

    public static final String COL_PASSWORD = "PASSWORD";

    public static final String COL_EMAIL = "EMAIL";

    public static final String COL_MOBILE = "MOBILE";

    public static final String COL_TELEPHONE = "TELEPHONE";
}
package com.dsprun.applications.srm.dao.impl;

import com.dsprun.applications.srm.dao.CommonDao;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class RmdbCommonDaoImpl implements CommonDao {
    @Override
    public String save(String entityName, Map entity) {
        return null;
    }

    @Override
    public int update(String entityName, Map entity) {
        return 0;
    }

    @Override
    public boolean deleteById(String entityName, String id) {
        return false;
    }

    @Override
    public List<Map> queryDatas(String entityName) {
        List<Map> list = new ArrayList();
        Map<String, String> data = new HashMap<>();
        data.put("planNo", "111111");
        data.put("planName", "XXX物资采购计划20201021");
        list.add(data);
        return list;
    }

    @Override
    public List<Map> queryDatas(String entityName, int start, int limit) {
        return null;
    }
}

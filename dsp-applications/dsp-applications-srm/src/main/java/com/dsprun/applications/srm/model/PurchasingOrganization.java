package com.dsprun.applications.srm.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

/**
 * @author Zhao Jun
 * 2020/11/1 19:54
 */
@Data
@TableName(value = "PURCHASING_ORGANIZATION")
public class PurchasingOrganization {
    /**
     * ID
     */
    @TableId(value = "ID", type = IdType.INPUT)
    private Integer id;

    /**
     * 父 ID
     */
    @TableField(value = "PID")
    private Integer pid;

    /**
     * 名称
     */
    @TableField(value = "\"NAME\"")
    private String name;

    /**
     * 编码
     */
    @TableField(value = "CODE")
    private String code;

    /**
     * 是否已删除 0:有效 1: 逻辑删除
     */
    @TableField(value = "IS_DELETE")
    private Short isDelete;

    /**
     * 创建时间
     */
    @TableField(value = "CREATE_TIME")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField(value = "UPDATE_TIME")
    private Date updateTime;

    /**
     * 创建用户
     */
    @TableField(value = "CREATE_USER")
    private String createUser;

    /**
     * 修改用户
     */
    @TableField(value = "UPDATE_USER")
    private String updateUser;

    public static final String COL_ID = "ID";

    public static final String COL_PID = "PID";

    public static final String COL_NAME = "NAME";

    public static final String COL_CODE = "CODE";

    public static final String COL_IS_DELETE = "IS_DELETE";

    public static final String COL_CREATE_TIME = "CREATE_TIME";

    public static final String COL_UPDATE_TIME = "UPDATE_TIME";

    public static final String COL_CREATE_USER = "CREATE_USER";

    public static final String COL_UPDATE_USER = "UPDATE_USER";
}
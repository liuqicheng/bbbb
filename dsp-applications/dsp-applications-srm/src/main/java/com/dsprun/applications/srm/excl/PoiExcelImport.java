package com.dsprun.applications.srm.excl;



import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
/**
 * 
 * @author sheny
 *
 */
public class PoiExcelImport {

	 //%%%%%%%%-------常量部分 开始----------%%%%%%%%%  
    /**
     * 默认的开始读取的行位置为第一行（索引值为0） 
     */
    private final static int READ_START_POS = 1;  
    
    //正则表达式 用于匹配属性的第一个字母
    private static final String REGEX = "[a-zA-Z]";
      
    /**
     * 默认结束读取的行位置为最后一行（索引值=0，用负数来表示倒数第n行） 
     */
    private final static int READ_END_POS = 0;  
      
    /**
     * 默认Excel内容的开始比较列位置为第一列（索引值为0） 
     */
    private final static int COMPARE_POS = 0;  
      
    /**
     * 默认多文件合并的时需要做内容比较（相同的内容不重复出现） 
     */
    private final static boolean NEED_COMPARE = true;  
      
    /**
     * 默认多文件合并的新文件遇到名称重复时，进行覆盖 
     */
    private final static boolean NEED_OVERWRITE = true;  
      
    /**
     * 默认只操作一个sheet 
     */
    private final static boolean ONLY_ONE_SHEET = true;  
      
    /**
     * 默认读取第一个sheet中（只有当ONLY_ONE_SHEET = true时有效） 
     */
    private final static int SELECTED_SHEET = 0;  
      
    /**
     * 默认从第一个sheet开始读取（索引值为0） 
     */
    private final static int READ_START_SHEET= 0;  
      
    /**
     * 默认在最后一个sheet结束读取（索引值=0，用负数来表示倒数第n行） 
     */
    private final static int READ_END_SHEET = 0;  
      
    /**
     * 默认打印各种信息 
     */
    private final static boolean PRINT_MSG = true;  
      
    //%%%%%%%%-------常量部分 结束----------%%%%%%%%%  
      
  
    //%%%%%%%%-------字段部分 开始----------%%%%%%%%%  
    /**
     * Excel文件路径 
     */
    private String excelPath = "data.xlsx";  
  
    /**
     * 设定开始读取的位置，默认为0 
     */
    private static int startReadPos = READ_START_POS;  
  
    /**
     * 设定结束读取的位置，默认为0，用负数来表示倒数第n行 
     */
    private static int endReadPos = READ_END_POS;  
      
    /**
     * 设定开始比较的列位置，默认为0 
     */
    private int comparePos = COMPARE_POS;  
  
    /**
     *  设定汇总的文件是否需要替换，默认为true 
     */
    private boolean isOverWrite = NEED_OVERWRITE;  
      
    /**
     *  设定是否需要比较，默认为true(仅当不覆写目标内容是有效，即isOverWrite=false时有效) 
     */
    private boolean isNeedCompare = NEED_COMPARE;  
      
    /**
     * 设定是否只操作第一个sheet 
     */
    private static boolean onlyReadOneSheet = ONLY_ONE_SHEET;  
      
    /**
     * 设定操作的sheet在索引值 
     */
    private static int selectedSheetIdx =SELECTED_SHEET;  
      
    /**
     * 设定操作的sheet的名称 
     */
    private static String selectedSheetName = "";  
      
    /**
     * 设定开始读取的sheet，默认为0 
     */
    private static int startSheetIdx = READ_START_SHEET;  
  
    /**
     * 设定结束读取的sheet，默认为0，用负数来表示倒数第n行     
     */
    private static int endSheetIdx = READ_END_SHEET;  
      
    /**
     * 设定是否打印消息 
     */
    private static boolean printMsg = PRINT_MSG;  
      
      
    //%%%%%%%%-------字段部分 结束----------%%%%%%%%%  
      
      
    public PoiExcelImport(){  
          
    }  
      
    public PoiExcelImport(String excelPath){  
        this.excelPath = excelPath;  
    }  
      
    /**
     * 还原设定（其实是重新new一个新的对象并返回） 
     * @return 
     */
    public PoiExcelImport RestoreSettings(){  
        PoiExcelImport instance = new  PoiExcelImport(this.excelPath);  
        return instance;  
    }  
      
    /**
     * 自动根据文件扩展名，调用对应的读取方法 
     *  
     * @Title: writeExcel 
     * @Date : 2014-9-11 下午01:50:38 
     * @param  //xlsPath
     * @throws IOException 
     */
    public List<?> readExcel(Class<?> clazz) throws IOException{  
        return readExcel(this.excelPath,clazz);  
    }  
  
    /**
     * 自动根据文件扩展名，调用对应的读取方法 
     *  
     * @Title: writeExcel 
     * @Date : 2014-9-11 下午01:50:38 
     * @param xlsPath 
     * @throws IOException 
     */
    public static List<?> readExcel(String xlsPath,Class<?> clazz) throws IOException{  
          
        //扩展名为空时，  
        if (xlsPath.equals("")){  
            throw new IOException("文件路径不能为空！");  
        }else{  
            File file = new File(xlsPath);  
            if(!file.exists()){  
                throw new IOException("文件不存在！");  
            }  
        }  
          
        //获取扩展名  
        String ext = xlsPath.substring(xlsPath.lastIndexOf(".")+1);  
          
        try {  
              
            if("xls".equals(ext)){              //使用xls方式读取  
                return readExcel_xls(xlsPath,clazz);  
            }else if("xlsx".equals(ext)){       //使用xlsx方式读取  
                return readExcel_xlsx(xlsPath,clazz);  
            }else{                                  //依次尝试xls、xlsx方式读取  
                try{  
                    return readExcel_xls(xlsPath,clazz);  
                } catch (IOException e1) {  
                    try{  
                        return readExcel_xlsx(xlsPath,clazz);  
                    } catch (IOException e2) {  
                        throw e2;  
                    }  
                }  
            }  
        } catch (IOException e) {  
            throw e;  
        }  
    }  
    
  
    /**
     * //读取Excel 2007版，xlsx格式 
     *  
     * @Title: readExcel_xlsx 
     * @Date :
     * @return 
     * @throws IOException 
     */
    public List<Object> readExcel_xlsx(Class<?> clazz) throws IOException {  
        return readExcel_xlsx(excelPath,clazz);  
    }  
  
    /**
     * //读取Excel 2007版，xlsx格式 
     *  
     * @Title: readExcel_xlsx 
     * @Date :
     * @return 
     * @throws Exception 
     */
    public static List<Object> readExcel_xlsx(String xlsPath,Class<?> clazz) throws IOException {  
        // 判断文件是否存在  
        File file = new File(xlsPath);  
        if (!file.exists()) {  
            throw new IOException("文件名为" + file.getName() + "Excel文件不存在！");  
        }  
  
        XSSFWorkbook wb = null;  
        List<Row> rowList = new ArrayList<Row>();  
        try {  
            FileInputStream fis = new FileInputStream(file);  
            // 去读Excel  
            wb = new XSSFWorkbook(fis);  
  
            // 读取Excel 2007版，xlsx格式  
            rowList = readExcel(wb,clazz);  
  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
        return returnObjectList(rowList,clazz);  
    }  
  
    /***
     * 读取Excel(97-03版，xls格式) 
     *  
     * @throws IOException 
     *  
     * @Title: readExcel 
     * @Date : 2014-9-11 上午09:53:21 
     */
    public List<Object> readExcel_xls(Class<?> clazz) throws IOException {  
        return readExcel_xls(excelPath,clazz);  
    }  
  
    /***
     * 读取Excel(97-03版，xls格式) 
     *  
     * @throws Exception 
     *  
     * @Title: readExcel 
     * @Date : 2014-9-11 上午09:53:21 
     */
    public static List<Object> readExcel_xls(String xlsPath,Class<?> clazz) throws IOException {  
  
        // 判断文件是否存在  
        File file = new File(xlsPath);  
        if (!file.exists()) {  
            throw new IOException("文件名为" + file.getName() + "Excel文件不存在！");  
        }  
  
        HSSFWorkbook wb = null;// 用于Workbook级的操作，创建、删除Excel  
        List<Row> rowList = new ArrayList<Row>();  
  
        try {  
            // 读取Excel  
            wb = new HSSFWorkbook(new FileInputStream(file));  
  
            // 读取Excel 97-03版，xls格式  
            rowList = readExcel(wb,clazz);  
  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
        return returnObjectList(rowList,clazz);  
    }  
  
    /***
     * 读取单元格的值 
     *  
     * @Title: getCellValue 
     * @Date :
     * @param cell 
     * @return 
     */
    private static String getCellValue(Cell cell) {
        Object result = "";  
        if (cell != null) {
            switch (cell.getCellType()) {
                case STRING:
                result = cell.getStringCellValue();
                break;  
                case NUMERIC:
            	if(HSSFDateUtil.isCellDateFormatted(cell)){
            		Date d = cell.getDateCellValue();
            		DateFormat formater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            		result = formater.format(d);
            	}else{
            		DecimalFormat df = new DecimalFormat("########");
            		result = df.format(cell.getNumericCellValue());
            	}
                
                break;  
                case BOOLEAN:
                result = cell.getBooleanCellValue();  
                break;  
                case FORMULA:
                result = cell.getCellFormula();  
                break;  
                case ERROR:
                result = cell.getErrorCellValue();  
                break;  
                case BLANK:
                break;  
            default:  
                break;  
            }  
        }  
        return result.toString();  
    }  
  
    /**
     * 通用读取Excel 
     *  
     * @Title: readExcel 
     * @Date : 2014-9-11 上午11:26:53 
     * @param wb 
     * @return 
     */
    private static List<Row> readExcel(Workbook wb,Class<?> clazz) {  
        List<Row> rowList = new ArrayList<Row>();  
          
        int sheetCount = 1;//需要操作的sheet数量  
          
        Sheet sheet = null;  
        if(onlyReadOneSheet){   //只操作一个sheet  
            // 获取设定操作的sheet(如果设定了名称，按名称查，否则按索引值查)  
            sheet =selectedSheetName.equals("")? wb.getSheetAt(selectedSheetIdx):wb.getSheet(selectedSheetName);  
        }else{                          //操作多个sheet  
            sheetCount = wb.getNumberOfSheets();//获取可以操作的总数量  
        }  
          
        // 获取sheet数目  
        for(int t=startSheetIdx; t<sheetCount+endSheetIdx;t++){  
            // 获取设定操作的sheet  
            if(!onlyReadOneSheet) {  
                sheet =wb.getSheetAt(t);  
            }  
              
            //获取最后行号  
            int lastRowNum = sheet.getLastRowNum();  
  
            Row row = null;  
            // 循环读取  
            for (int i = startReadPos; i <= lastRowNum + endReadPos; i++) {  
                row = sheet.getRow(i);  
                if (row != null) {  
                    rowList.add(row);  
                }  
            }  
        }  
        return  rowList;  
    }  
    
    private static List<Object> returnObjectList(List<Row> rowList,Class<?> clazz) {
        List<Object> objectList=null;
        Object obj=null;
        String attribute=null;
        String value=null;
        int j=0;
        try {   
            objectList=new ArrayList<Object>();
            Field[] declaredFields = clazz.getDeclaredFields();
            for (Row row : rowList) {
                j=0;
                obj = clazz.newInstance();
                for (Field field : declaredFields) {    
                    attribute=field.getName().toString();
                    value = getCellValue(row.getCell(j));
                    setAttrributeValue(obj,attribute,value);    
                    j++;
                }
                objectList.add(obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return objectList;
    }
    
    private static void setAttrributeValue(Object obj,String attribute,String value) {
        //得到该属性的set方法名
        String method_name = convertToMethodName(attribute,obj.getClass(),true);
        Method[] methods = obj.getClass().getMethods();
        for (Method method : methods) {
            /**
             * 因为这里只是调用bean中属性的set方法，属性名称不能重复
             * 所以set方法也不会重复，所以就直接用方法名称去锁定一个方法
             * （注：在java中，锁定一个方法的条件是方法名及参数）
            */
            if(method.getName().equals(method_name))
            {
                Class<?>[] parameterC = method.getParameterTypes();
                try {
                    /**如果是(整型,浮点型,布尔型,字节型,时间类型),
                     * 按照各自的规则把value值转换成各自的类型
                     * 否则一律按类型强制转换(比如:String类型)
                    */
                    if(parameterC[0] == int.class || parameterC[0]== Integer.class)
                    {
                    	if(value.indexOf(".")!=-1){
                    		value = value.substring(0, value.lastIndexOf("."));
                    	}
                    	if(!"".equals(value)){
                    		method.invoke(obj,Integer.valueOf(value));
                    	}
                        
                        break;
                    }else if(parameterC[0] == float.class || parameterC[0]== Float.class){
                        method.invoke(obj, Float.valueOf(value));
                        break;
                    }else if(parameterC[0] == double.class || parameterC[0]== Double.class)
                    {
                        method.invoke(obj, Double.valueOf(value));
                        break;
                    }else if(parameterC[0] == byte.class || parameterC[0]== Byte.class)
                    {
                        method.invoke(obj, Byte.valueOf(value));
                        break;
                    }else if(parameterC[0] == boolean.class|| parameterC[0]== Boolean.class)
                    {
                        method.invoke(obj, Boolean.valueOf(value));
                        break;
                    }else if(parameterC[0] == Date.class)
                    {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        Date date=null;
                        try {
                            date=sdf.parse(value);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        method.invoke(obj,date);
                        break;
                    }else
                    {
                        method.invoke(obj,parameterC[0].cast(value));
                        break;
                    }
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (SecurityException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    /**
     * 功能:根据属性生成对应的set/get方法
     */
    private static String convertToMethodName(String attribute,Class<?> objClass,boolean isSet) {
        /** 通过正则表达式来匹配第一个字符 **/
        Pattern p = Pattern.compile(REGEX);
        Matcher m = p.matcher(attribute);
        StringBuilder sb = new StringBuilder();
        /** 如果是set方法名称 **/
        if(isSet)
        {
            sb.append("set");
        }else{
        /** get方法名称 **/
            try {
                Field attributeField = objClass.getDeclaredField(attribute);
                /** 如果类型为boolean **/
                if(attributeField.getType() == boolean.class||attributeField.getType() == Boolean.class)
                {
                    sb.append("is");
                }else
                {
                    sb.append("get");
                }
            } catch (SecurityException e) {
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
        }
        /** 针对以下划线开头的属性 **/
        if(attribute.charAt(0)!='_' && m.find())
        {
            sb.append(m.replaceFirst(m.group().toUpperCase()));
        }else{
            sb.append(attribute);
        }
        return sb.toString();
    }
  
  
    /**
     * 复制一个单元格样式到目的单元格样式 
     *  
     * @param fromStyle 
     * @param toStyle 
     */
    public static void copyCellStyle(CellStyle fromStyle, CellStyle toStyle) {  
        toStyle.setAlignment(fromStyle.getAlignment());  
        // 边框和边框颜色  
        toStyle.setBorderBottom(fromStyle.getBorderBottom());  
        toStyle.setBorderLeft(fromStyle.getBorderLeft());  
        toStyle.setBorderRight(fromStyle.getBorderRight());  
        toStyle.setBorderTop(fromStyle.getBorderTop());  
        toStyle.setTopBorderColor(fromStyle.getTopBorderColor());  
        toStyle.setBottomBorderColor(fromStyle.getBottomBorderColor());  
        toStyle.setRightBorderColor(fromStyle.getRightBorderColor());  
        toStyle.setLeftBorderColor(fromStyle.getLeftBorderColor());  
  
        // 背景和前景  
        toStyle.setFillBackgroundColor(fromStyle.getFillBackgroundColor());  
        toStyle.setFillForegroundColor(fromStyle.getFillForegroundColor());  
  
        // 数据格式  
        toStyle.setDataFormat(fromStyle.getDataFormat());  
        toStyle.setFillPattern(fromStyle.getFillPattern());  
        // toStyle.setFont(fromStyle.getFont(null));  
        toStyle.setHidden(fromStyle.getHidden());  
        toStyle.setIndention(fromStyle.getIndention());// 首行缩进  
        toStyle.setLocked(fromStyle.getLocked());  
        toStyle.setRotation(fromStyle.getRotation());// 旋转  
        toStyle.setVerticalAlignment(fromStyle.getVerticalAlignment());  
        toStyle.setWrapText(fromStyle.getWrapText());  
  
    }  
  
    /**
     * 获取合并单元格的值 
     *  
     * @param sheet 
     * @param row 
     * @param column 
     * @return 
     */
    public void setMergedRegion(Sheet sheet) {  
        int sheetMergeCount = sheet.getNumMergedRegions();  
  
        for (int i = 0; i < sheetMergeCount; i++) {  
            // 获取合并单元格位置  
            CellRangeAddress ca = sheet.getMergedRegion(i);  
            int firstRow = ca.getFirstRow();  
            if (startReadPos - 1 > firstRow) {// 如果第一个合并单元格格式在正式数据的上面，则跳过。  
                continue;  
            }  
            int lastRow = ca.getLastRow();  
            int mergeRows = lastRow - firstRow;// 合并的行数  
            int firstColumn = ca.getFirstColumn();  
            int lastColumn = ca.getLastColumn();  
            // 根据合并的单元格位置和大小，调整所有的数据行格式，  
            for (int j = lastRow + 1; j <= sheet.getLastRowNum(); j++) {  
                // 设定合并单元格  
                sheet.addMergedRegion(new CellRangeAddress(j, j + mergeRows, firstColumn, lastColumn));  
                j = j + mergeRows;// 跳过已合并的行  
            }  
  
        }  
    }  
      
  
   /* public String getExcelPath() {
        return this.excelPath;  
    }  
  
    public void setExcelPath(String excelPath) {  
        this.excelPath = excelPath;  
    }  
  
    public boolean isNeedCompare() {  
        return isNeedCompare;  
    }  
  
    public void setNeedCompare(boolean isNeedCompare) {  
        this.isNeedCompare = isNeedCompare;  
    }  
  
    public int getComparePos() {  
        return comparePos;  
    }  
  
    public void setComparePos(int comparePos) {  
        this.comparePos = comparePos;  
    }  
  
    public int getStartReadPos() {  
        return startReadPos;  
    }  
  
    public int getEndReadPos() {  
        return endReadPos;  
    }  
  
    public void setOverWrite(boolean isOverWrite) {  
        this.isOverWrite = isOverWrite;  
    }  
  
    public boolean isOnlyReadOneSheet() {  
        return onlyReadOneSheet;  
    }  
  
    public int getSelectedSheetIdx() {  
        return selectedSheetIdx;  
    }  
  
  
    public String getSelectedSheetName() {  
        return selectedSheetName;  
    }  
  
  
    public int getStartSheetIdx() {  
        return startSheetIdx;  
    }  
  
  
    public int getEndSheetIdx() {  
        return endSheetIdx;  
    }  
  
  
    public boolean isPrintMsg() {  
        return printMsg;  
    }  
  */
	
}

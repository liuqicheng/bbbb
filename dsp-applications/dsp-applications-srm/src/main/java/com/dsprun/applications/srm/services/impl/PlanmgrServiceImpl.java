package com.dsprun.applications.srm.services.impl;

import com.dsprun.applications.srm.dao.CommonDao;
import com.dsprun.applications.srm.services.PlanmgrService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class PlanmgrServiceImpl implements PlanmgrService{

    @Resource(name = "rmdbCommonDaoImpl")
    CommonDao rmdbCommonDao;

    @Override
    public List<Map> getPlans() {
       return rmdbCommonDao.queryDatas("srm_plan");
    }
}

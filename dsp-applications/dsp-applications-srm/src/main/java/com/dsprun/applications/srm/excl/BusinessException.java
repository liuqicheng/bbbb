package com.dsprun.applications.srm.excl;



@SuppressWarnings("serial")
public class BusinessException extends Exception {

    private String code;

    private Object[] parmetters;

    public BusinessException() {
        super();
    }

    public BusinessException(String code) {
        super();
        this.code = code;
    }

    public BusinessException(String code, Throwable cause) {
        super(cause);
        this.code = code;
    }

    public BusinessException(String code, Object[] parmetters) {
        super();
        this.code = code;
        this.parmetters = parmetters;
    }

    public BusinessException(String code, Throwable cause, Object[] parmetters) {
        super(cause);
        this.code = code;
        this.parmetters = parmetters;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object[] getParmetters() {
        return parmetters;
    }

    public void setParmetters(Object[] parmetters) {
        this.parmetters = parmetters;
    }

}

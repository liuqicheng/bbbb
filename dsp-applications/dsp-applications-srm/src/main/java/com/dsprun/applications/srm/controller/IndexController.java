package com.dsprun.applications.srm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

    @RequestMapping("/index")
    public String index(Model model){
        model.addAttribute("test", "zdc");
        return "/srm/index";
    }

    @RequestMapping("/index1")
    public String index1(Model model){
        //model.addAttribute("test", "zdc");
        return "/srm/inde";
    }
}

package com.dsprun.applications.srm.services;

import com.dsprun.applications.srm.model.SupplierDetail;

import java.util.List;

public interface SupplierDetailService {
    List<SupplierDetail> selectSupplierInfoBySupplierId(Integer supplierId);

    List<SupplierDetail> selectAll();

    SupplierDetail findSupplierDetailBySupplierDetailId(Integer valueOf);
}

package com.dsprun.applications.srm.services;

import com.dsprun.applications.srm.model.PlanExec;
import com.dsprun.applications.srm.model.PlanExecDetail;
import com.dsprun.applications.srm.model.Project;
import com.dsprun.applications.srm.vo.PlanExecVO;

import java.util.List;

public interface PlanExecTaskService {

    List<PlanExecVO> selectExecAll();

    PlanExecVO selectByPrimaryKey(Integer valueOf);

    List<PlanExec> getRePlanExecListByPlanExec(PlanExec planExec);

    List<PlanExecDetail> getRePlanExecDetailVOListByPrimaryKey(Integer valueOf);

    PlanExec findPlanExecById(Integer valueOf);

    List<PlanExecDetail> findPlanExecDetailList(Integer id);

}

package com.dsprun.applications.srm.services.impl;

import com.dsprun.applications.srm.mapper.AreaMapper;
import com.dsprun.applications.srm.mapper.PlanExecDetailMapper;
import com.dsprun.applications.srm.mapper.PlanExecMapper;
import com.dsprun.applications.srm.mapper.ProjectMapper;
import com.dsprun.applications.srm.model.Area;
import com.dsprun.applications.srm.model.PlanExec;
import com.dsprun.applications.srm.model.PlanExecDetail;
import com.dsprun.applications.srm.model.Project;
import com.dsprun.applications.srm.services.PlanExecTaskService;
import com.dsprun.applications.srm.services.ProjectService;
import com.dsprun.applications.srm.vo.PlanExecVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class PlanExecTaskServiceImpl implements PlanExecTaskService {

    @Autowired
   private PlanExecDetailMapper planExecDetailMapper;
    
    @Resource
    private PlanExecMapper planExecMapper;

    @Autowired
    private ProjectMapper ProjectMapper;

    @Autowired
    private AreaMapper areaMapper;
    @Override
    public List<PlanExecVO> selectExecAll() {
        List<PlanExecVO> execDataAll = planExecMapper.selectExecAll();

        return null;
    }

    @Override
    public PlanExecVO selectByPrimaryKey(Integer id) {

        //会先相关数据
        PlanExecVO planExecVO = planExecMapper.findPlanExecByExecId(id);

        //查看物料信息
        planExecVO.setPlanExecDetail(planExecDetailMapper.getRePlanExecDetailVOListByPrimaryKey(Integer.valueOf(id)));


        // 查找所有的省
        List<Area> allProvinces = areaMapper.findAllProvinces();
        List<Area> allCitys = null;
        List<Area> allCountys = null;
        if (allProvinces != null && allProvinces.size() > 0) {
            // 查找当前省下的所有市
            allCitys = areaMapper.findAllChildrenAOBean(allProvinces.get(0).getId());
            if (allCitys != null && allCitys.size() > 0) {
                // 查找当前市下的所有区县
                allCountys = areaMapper.findAllChildrenAOBean(allCitys.get(0).getId());
            }
        }

        return null;
    }

























    @Override
    public List<PlanExec> getRePlanExecListByPlanExec(PlanExec planExec) {
        return null;
    }

    @Override
    public List<PlanExecDetail> getRePlanExecDetailVOListByPrimaryKey(Integer id) {

      /*  HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("id", id);*/
        List<PlanExecDetail> planExecDetails = (List<PlanExecDetail>) planExecDetailMapper.getRePlanExecDetailVOListByPrimaryKey(id);
        return planExecDetails;
    }

    @Override
    public PlanExec findPlanExecById(Integer valueOf) {
        return null;
    }

    @Override
    public List<PlanExecDetail> findPlanExecDetailList(Integer id) {

        List<PlanExecDetail> planExecDetails =  planExecDetailMapper.findPlanExecDetailByExecId(id);
        return null;
    }


}

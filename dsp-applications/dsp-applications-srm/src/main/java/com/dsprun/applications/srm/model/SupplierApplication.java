package com.dsprun.applications.srm.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "supplier_application")
public class SupplierApplication {
    @TableId(value = "ID", type = IdType.INPUT)
    private String id;

    @TableField(value = "APPLICATION_TYPE")
    private String applicationType;

    @TableField(value = "SUPPLIER_ID")
    private String supplierId;

    @TableField(value = "SUPPLIER_ACCOUNT")
    private String supplierAccount;

    @TableField(value = "SUPPLIER_DETAIL_ID")
    private String supplierDetailId;

    @TableField(value = "APPLICATION_COMPANY_ID")
    private String applicationCompanyId;

    @TableField(value = "APPLICATION_COMPANY_NAME")
    private String applicationCompanyName;

    @TableField(value = "ITEM_DESCRIPTION")
    private String itemDescription;

    @TableField(value = "APPLICATION_REASON")
    private String applicationReason;

    @TableField(value = "REMARK")
    private String remark;

    @TableField(value = "\"STATUS\"")
    private String status;

    @TableField(value = "CREATE_TIME")
    private Date createTime;

    public static final String COL_ID = "ID";

    public static final String COL_APPLICATION_TYPE = "APPLICATION_TYPE";

    public static final String COL_SUPPLIER_ID = "SUPPLIER_ID";

    public static final String COL_SUPPLIER_ACCOUNT = "SUPPLIER_ACCOUNT";

    public static final String COL_SUPPLIER_DETAIL_ID = "SUPPLIER_DETAIL_ID";

    public static final String COL_APPLICATION_COMPANY_ID = "APPLICATION_COMPANY_ID";

    public static final String COL_APPLICATION_COMPANY_NAME = "APPLICATION_COMPANY_NAME";

    public static final String COL_ITEM_DESCRIPTION = "ITEM_DESCRIPTION";

    public static final String COL_APPLICATION_REASON = "APPLICATION_REASON";

    public static final String COL_REMARK = "REMARK";

    public static final String COL_STATUS = "STATUS";

    public static final String COL_CREATE_TIME = "CREATE_TIME";
}
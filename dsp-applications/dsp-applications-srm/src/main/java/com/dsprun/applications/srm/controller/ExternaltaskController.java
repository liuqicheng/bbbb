package com.dsprun.applications.srm.controller;

import com.dsprun.applications.srm.model.ExtPlan;
import com.dsprun.applications.srm.model.ExtPlanDetail;
import com.dsprun.applications.srm.services.ExternaltaskDetailService;
import com.dsprun.applications.srm.services.ExternaltaskService;
import com.dsprun.applications.srm.vo.ExtPlanDetailVO;
import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("dsp/taskmanager/externaltask")
public class ExternaltaskController {

    @Autowired
     ExternaltaskService externaltaskService;

    @Autowired
     ExternaltaskDetailService externaltaskDetailService;
//
//
//    @ResponseBody
//    @GetMapping(value = "/listView")
//    public String listView(HttpServletRequest request, HttpSession session) {
//        System.out.println("获取外部计划信息！");
//        ModelMap modelMap = new ModelMap();
//        // 查看全部外部计划
//        List<ExtPlan> extPlanData = externaltaskService.selectAll();
//        modelMap.put("extPlanData",extPlanData);
//        return "extPlanData";
//    }
//
//
//
//    /*
//     * 外部平台点击查看
//     *
//     *
//     * */
//    @GetMapping(value = "/viewItem")
//    public String viewItem(ModelMap modelMap,HttpServletRequest request,@RequestParam(name = "planBatchNo",required = false)String planBatchNo){
//        System.out.println("跳转到外部计划详细页面！");
//        ExtPlanDetailVO plan = null;
//        if(planBatchNo != null && !"".equals(planBatchNo)){
//             plan = externaltaskService.findExtPlanById(planBatchNo);
//        }
//        modelMap.put("ExtPlanDetailVO",plan);
//       System.out.println(modelMap);
//        return "/srm/index";
//    }

        /*
        * 外部平台点击查看更多
        *
        *
        * */
   /* @GetMapping(value = "/viewItem/{planBatchNo}")
    public String viewItemDetail(ModelMap modelMap,HttpServletRequest request,@PathVariable(name = "planBatchNo")String planBatchNo){
        System.out.println("跳转到外部计划详细页面！");
        if(planBatchNo != null && !"".equals(planBatchNo)){
           // ExtPlan plan = externaltaskService.findExtPlanById(planBatchNo);
            List<ExtPlanDetail> planDetailList = externaltaskDetailService.findExtPlanDetailByPlanBatchNo(planBatchNo);
           // modelMap.put("plan", plan);
            modelMap.put("planDetailList", planDetailList);
        }
        return "srm/index";
    }*/

}

package com.dsprun.applications.srm.services.impl;

import com.dsprun.applications.srm.mapper.AreaMapper;
import com.dsprun.applications.srm.model.Area;
import com.dsprun.applications.srm.services.AreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AreaServiceImpl implements AreaService {

    @Autowired
    private AreaMapper areaMapper;
    @Override
    public List<Area> findAllProvincesAOBean() {

        List<Area> AllProvinces= areaMapper.findAllProvinces();
        return null;
    }

    @Override
    public List<Area> findAllChildrenAOBean(Integer id) {
        List<Area> AllProvinces= areaMapper.findAllChildrenAOBean(id);
        return null;
    }
}

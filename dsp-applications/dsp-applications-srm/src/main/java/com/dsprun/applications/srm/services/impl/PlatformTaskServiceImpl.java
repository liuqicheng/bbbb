package com.dsprun.applications.srm.services.impl;

import com.dsprun.applications.srm.mapper.PlanDetailMapper;
import com.dsprun.applications.srm.mapper.PlanMapper;
import com.dsprun.applications.srm.model.Plan;
import com.dsprun.applications.srm.model.PlanDetail;
import com.dsprun.applications.srm.services.PlatformTaskService;
import com.dsprun.applications.srm.vo.PlanVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class PlatformTaskServiceImpl implements PlatformTaskService {

    @Autowired
    private PlanMapper planMapper;

    @Autowired
    private PlanDetailMapper planDetailMapper;


    //@Override
    /*public ModelMap planExcelImport(MultipartFile planExcel, String planBatchNo) {
        ModelMap modelMap = new ModelMap();
        //获取当前登入人
        //SysUserPO user = (SysUserPO) request.getSession().getAttribute(SessionUtil.SESSION_PSM_USER);
        InputStream in = null;
        List<List<Object>> listob = null;
        // 如果只是上传一个文件,则只需要MultipartFile类型接收文件即可,而且无需显式指定@RequestParam注解
        if (planExcel.isEmpty()) {
            System.out.println("file is null");
        } else {
            try {
                in = planExcel.getInputStream();
                // Excel导入
                ImportExcel importExcel = new ImportExcel();
                int startRow = 0;
                int startCell = 1;
                // 取得Excel中的数据列表
                listob = importExcel.getBankListByExcel(in, planExcel.getOriginalFilename(), startRow, startCell);
                if (listob != null && listob.size() > 0) {
                    Plan plan = new Plan();
                    // 计划名称:
                    if (listob.get(0) != null && listob.get(0).size() == 2) {
                        plan.setPlan_name((String) listob.get(0).get(1));
                    } else {
                        throw new BusinessException("MSG1013E");
                    }
                    // 描述:
                    if (listob.get(1) != null && listob.get(1).size() == 2) {
                        plan.setDescription((String) listob.get(1).get(1));
                    } else {
                        plan.setDescription("");
                    }
                    // 需求公司:
                    if (listob.get(2) != null && listob.get(2).size() == 2) {
                        plan.setDemand_comp_name((String) listob.get(2).get(1));
                        String company_ID = companyService.findSysCompanyByCompanyName((String) listob.get(2).get(1)).getCOMPANY_ID();
                        plan.setDemand_comp_id(company_ID);
                    }
                    // 需求部门:
                    if (listob.get(3) != null && listob.get(3).size() == 2) {
                        plan.setDemand_org_name((String) listob.get(3).get(1));
                        //部门ID,暂时赋值为1
                        plan.setDemand_org_id(1);
                    } else {
                        modelMap.put("status", "failure");
                        modelMap.put("msg", "导入数据失败，请重新填写部门！");
                        return modelMap;
                    }
                    // 备考:
                    if (listob.get(4) != null && listob.get(4).size() == 2) {
                        plan.setComments((String) listob.get(4).get(1));
                    } else {
                        plan.setComments("");
                    }
                    // 暂时没有用，数据库中不允许为空，所以随便搞一下
                    plan.setPlan_date(new Date());
                    plan.setPlan_holder("");
                    // 加入planBean

                    startRow = 6;
                    listob = listob.subList(6, listob.size());
                    // 对数据内容进行转换
                    @SuppressWarnings("unchecked")
                    List<PlanDetailImportVO> pdbList = (List<PlanDetailImportVO>) (List<?>) importExcel.excelToBeanConvert("com.dsp.common.vo.PlanDetailImportVO", listob,
                            createExportExcelHeaders(), startRow);
                    List<PlanDetail> planDetailList = new ArrayList<PlanDetail>();
                    PlanDetail planDetail = null;
                    if (pdbList.size() > 0) {
                        Unit unit = null;
                        for (PlanDetailImportVO planDetailImportBean : pdbList) {
                            // 报价数据追加到数据库中
                            planDetail = new PlanDetailVO();
                            planDetail.setAmount(planDetailImportBean.getAmount());
                            planDetail.setArrival_date(planDetailImportBean.getArrivalDate());
                            planDetail.setComments(planDetailImportBean.getComments());
                            planDetail.setManufacturer(planDetailImportBean.getManufacturer());

                            planDetail.setMaterial_code(planDetailImportBean.getMaterialCode());
                            planDetail.setPlan_batch_no(planBatchNo);

                            planDetail.setMaterial_name(planDetailImportBean.getMaterialName());
                            planDetail.setPackage_amount(planDetailImportBean.getPackageAmount());

                            planDetail.setPo_num(planDetailImportBean.getPoNum());
                            planDetail.setPro_name(planDetailImportBean.getProName());
                            planDetail.setPur_cycle(planDetailImportBean.getPurCycle());
                            planDetail.setSale_contract_no(planDetailImportBean.getSaleContractNo());
                            planDetail.setSpn_typ(planDetailImportBean.getSpnTyp());
                            unit = unitService.findUnitByName(planDetailImportBean.getUnitName());
                            if (unit != null) {
                                planDetail.setUnit(unit.getId());
                            }
                            planDetailList.add(planDetail);
                        }
                    }
                    //如果批次号不为空
                    if (!"".equals(planBatchNo)&&planBatchNo!=null) {
                        // 如果原来的plan存在，先进行删除
//						this.deletePlan(planBatchNo, user.getName());
                        this.delPlan(planBatchNo);
                    }
                    // 保存计划及计划详细
                    boolean planFalg = this.addPlanAndPlanDetail(plan, planDetailList, user.getACCOUNT());
                    if(planFalg){
                        modelMap.put("status", "success");
                        modelMap.put("msg", "导入数据成功");
                    }else{
                        modelMap.put("status", "failure");
                        modelMap.put("msg", "导入数据失败");
                    }
                } else {
                    modelMap.put("status", "failure");
                    modelMap.put("msg", "没有导入数据");
                }
            }catch (Exception e) {
                e.printStackTrace();
                System.out.println("错误信息："+e);
                modelMap.put("status", "failure");
                modelMap.put("msg", "导入数据失败"+e);
            }finally {
                if (in != null) {
                    in.close();
                }
            }
        }
        return modelMap;
    }*/




















    @Override
    public List<Plan> selectAllPlatform() {
        List<Plan> planList = planMapper.selectList();
        return null;
    }

    @Override
    public List<PlanVO> findPlanById(String planBatchNo) {
        List<Plan> planList = planMapper.selectListByplanBatchNo(planBatchNo);

        PlanVO planDetailByPlanBatchNo = planDetailMapper.findPlanDetailByPlanBatchNo(planBatchNo);


        return null;
    }


    @Override
    public List<PlanDetail> findPlanDetailByPlanBatchNo(String planBatchNo) {
        PlanVO planDetailData =   planDetailMapper.findPlanDetailByPlanBatchNo(planBatchNo);
        return null;
    }


}

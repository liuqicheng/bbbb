package com.dsprun.applications.srm.services.impl;

import com.dsprun.applications.srm.mapper.ExtPlanDetailMapper;
import com.dsprun.applications.srm.mapper.ExtPlanMapper;
import com.dsprun.applications.srm.model.ExtPlan;
import com.dsprun.applications.srm.model.ExtPlanDetail;
import com.dsprun.applications.srm.services.ExternaltaskService;
import com.dsprun.applications.srm.vo.ExtPlanDetailVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExternaltaskServiceImpl implements ExternaltaskService {

    @Autowired
    private ExtPlanMapper extPlanMapper;

    @Autowired
    private ExtPlanDetailMapper extPlanDetailMapper;

    @Override
    public List<ExtPlan> selectAll() {
        List<ExtPlan> allExeData = extPlanMapper.selectAll();

        return allExeData;
    }

    @Override
    public ExtPlanDetailVO findExtPlanById(String planBatchNo) {

        ExtPlan allExeData = null;
        if(null != planBatchNo && planBatchNo.equals("")){
             allExeData = extPlanMapper.selectDataById(planBatchNo);
        }

        ExtPlanDetailVO extPlanDetailVO = extPlanDetailMapper.findExtPlanDetailByPlanBatchNo(planBatchNo);

      return  extPlanDetailVO;
    }
}

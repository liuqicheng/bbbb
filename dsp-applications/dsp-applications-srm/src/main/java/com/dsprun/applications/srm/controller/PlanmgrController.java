package com.dsprun.applications.srm.controller;

import com.dsprun.applications.srm.client.PlanmgrServiceClient;
import com.dsprun.applications.srm.services.PlanmgrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * 采购计划控制器
 * @author sudong
 */
@RestController
@RequestMapping("/planmgr/page")
public class PlanmgrController {

    @Autowired
    PlanmgrService planmgrService;

    @Autowired
    PlanmgrServiceClient client;

    @RequestMapping(path="/index")
    public ModelAndView index(HttpServletRequest request){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("plans", client.getPlans());
        modelAndView.setViewName("srm/planmgr/index");
        return modelAndView;
    }

    @RequestMapping(path="/index0")
    public ModelAndView index0(HttpServletRequest request){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("plans", planmgrService.getPlans());
        modelAndView.setViewName("srm/planmgr/index");
        return modelAndView;
    }
}

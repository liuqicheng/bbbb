package com.dsprun.applications.srm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dsprun.applications.srm.model.SysUsera;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SysUseraMapper extends BaseMapper<SysUsera> {
    List<SysUsera> selectALL(String buyerName);
}
package com.dsprun.applications.srm.api;

import com.dsprun.applications.srm.services.PlanmgrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * 采购计划控制器
 * @author sudong
 */
@RestController
@RequestMapping("/planmgr/api")
public class PlanmgrApi {

    @Autowired
    PlanmgrService planmgrService;

    @RequestMapping(path="/getPlans",method = {RequestMethod.GET,RequestMethod.POST})
    @ResponseBody
    public List<Map> getPlans(){
        return planmgrService.getPlans();
    }
}

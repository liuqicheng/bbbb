package com.dsprun.applications.srm.services;

import com.dsprun.applications.srm.model.SysUsera;

import java.util.List;

public interface UserService {
    List<SysUsera> findUserByName(String buyerName);
}

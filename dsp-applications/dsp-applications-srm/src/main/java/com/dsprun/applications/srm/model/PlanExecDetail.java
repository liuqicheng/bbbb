package com.dsprun.applications.srm.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "plan_exec_detail")
public class PlanExecDetail {
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.INPUT)
    private Integer id;

    /**
     * 计划执行 ID
     */
    @TableField(value = "plan_exec_id")
    private Integer planExecId;

    /**
     * 初始计划执行id
     */
    @TableField(value = "plan_exec_id_base")
    private Integer planExecIdBase;

    /**
     * 批次号
     */
    @TableField(value = "plan_batch_no")
    private String planBatchNo;

    /**
     * 物料编码
     */
    @TableField(value = "material_code")
    private String materialCode;

    /**
     * 物料名称
     */
    @TableField(value = "material_name")
    private String materialName;

    /**
     * 规格型号
     */
    @TableField(value = "spn_typ")
    private String spnTyp;

    /**
     * 计量单位
     */
    @TableField(value = "unit")
    private String unit;

    /**
     * 数量
     */
    @TableField(value = "amount")
    private BigDecimal amount;

    /**
     * 制造商
     */
    @TableField(value = "manufacturer")
    private String manufacturer;

    /**
     * 到货日期
     */
    @TableField(value = "arrival_date")
    private Date arrivalDate;

    /**
     * 采购周期
     */
    @TableField(value = "pur_cycle")
    private String purCycle;

    /**
     * 采购合同号
     */
    @TableField(value = "sale_contract_no")
    private String saleContractNo;

    /**
     * 单包装数量
     */
    @TableField(value = "package_amount")
    private BigDecimal packageAmount;

    /**
     * 项目名称
     */
    @TableField(value = "pro_name")
    private String proName;

    /**
     * 生产订单号
     */
    @TableField(value = "po_num")
    private String poNum;

    /**
     * 采购形式
     */
    @TableField(value = "buy_type")
    private BigDecimal buyType;

    /**
     * 采购人
     */
    @TableField(value = "buyer")
    private String buyer;

    /**
     * 创建人
     */
    @TableField(value = "create_user")
    private String createUser;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 修改人
     */
    @TableField(value = "update_user")
    private String updateUser;

    /**
     * 修改时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    /**
     * 删除flag0:有效 1：逻辑删除
     */
    @TableField(value = "cancel_flag")
    private BigDecimal cancelFlag;

    /**
     * 备注
     */
    @TableField(value = "comments")
    private String comments;

    public static final String COL_ID = "id";

    public static final String COL_PLAN_EXEC_ID = "plan_exec_id";

    public static final String COL_PLAN_EXEC_ID_BASE = "plan_exec_id_base";

    public static final String COL_PLAN_BATCH_NO = "plan_batch_no";

    public static final String COL_MATERIAL_CODE = "material_code";

    public static final String COL_MATERIAL_NAME = "material_name";

    public static final String COL_SPN_TYP = "spn_typ";

    public static final String COL_UNIT = "unit";

    public static final String COL_AMOUNT = "amount";

    public static final String COL_MANUFACTURER = "manufacturer";

    public static final String COL_ARRIVAL_DATE = "arrival_date";

    public static final String COL_PUR_CYCLE = "pur_cycle";

    public static final String COL_SALE_CONTRACT_NO = "sale_contract_no";

    public static final String COL_PACKAGE_AMOUNT = "package_amount";

    public static final String COL_PRO_NAME = "pro_name";

    public static final String COL_PO_NUM = "po_num";

    public static final String COL_BUY_TYPE = "buy_type";

    public static final String COL_BUYER = "buyer";

    public static final String COL_CREATE_USER = "create_user";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_UPDATE_USER = "update_user";

    public static final String COL_UPDATE_TIME = "update_time";

    public static final String COL_CANCEL_FLAG = "cancel_flag";

    public static final String COL_COMMENTS = "comments";
}
package com.dsprun.applications.srm.services.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.dsprun.applications.srm.mapper.ProjectAddressMapper;
import com.dsprun.applications.srm.mapper.ProjectMapper;
import com.dsprun.applications.srm.model.Project;
import com.dsprun.applications.srm.model.ProjectAddress;
import com.dsprun.applications.srm.services.ProjectService;
import com.dsprun.applications.srm.vo.ProjectVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectMapper projectMapper;

    @Autowired
    private ProjectAddressMapper projectAddressMapper;

    @Override
    public void save(Project projectData, ProjectAddress projectAddr) {
        //List<ProjectVO> projectVOS = JSON.parseArray(projectData, ProjectVO.class);

            projectMapper.save(projectData);

            projectAddressMapper.save(projectAddr);
    }
}

package com.dsprun.applications.srm.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "PROJECT")
public class Project {
    @TableId(value = "ID", type = IdType.INPUT)
    private Integer id;

    @TableField(value = "PRO_NAME")
    private String proName;

    @TableField(value = "PUBLISH_DATE")
    private Date publishDate;

    @TableField(value = "PRO_TYPE")
    private BigDecimal proType;

    @TableField(value = "PLAN_EXEC_ID")
    private Integer planExecId;

    @TableField(value = "PLAN_BATCH_NOS")
    private String planBatchNos;

    @TableField(value = "PLAN_BATCH_NAMES")
    private String planBatchNames;

    @TableField(value = "SETTLEMENT_METHOD")
    private String settlementMethod;

    @TableField(value = "PUR_CONDITION")
    private String purCondition;

    @TableField(value = "DESCRIPTION")
    private String description;

    @TableField(value = "CONTACT")
    private String contact;

    @TableField(value = "CONTACT_PHONE")
    private String contactPhone;

    @TableField(value = "CONTACT_TEL")
    private String contactTel;

    @TableField(value = "CONTACT_TEL_EXTENSION")
    private String contactTelExtension;

    @TableField(value = "SUPPLIER_SELECT_IDS")
    private String supplierSelectIds;

    @TableField(value = "ENQUIRY_START_DATE")
    private Date enquiryStartDate;

    @TableField(value = "ENQUIRY_END_DATE")
    private Date enquiryEndDate;

    @TableField(value = "ENTRY_START_DATE")
    private Date entryStartDate;

    @TableField(value = "ENTRY_END_DATE")
    private Date entryEndDate;

    @TableField(value = "BIDDING_START_DATE")
    private Date biddingStartDate;

    @TableField(value = "BIDDING_END_DATE")
    private Date biddingEndDate;

    @TableField(value = "SHOW_END_DATE")
    private Date showEndDate;

    @TableField(value = "DEPOSIT")
    private BigDecimal deposit;

    @TableField(value = "BID_FEE")
    private BigDecimal bidFee;

    @TableField(value = "BID_SERVICE_FEE")
    private BigDecimal bidServiceFee;

    @TableField(value = "SPONSOR")
    private String sponsor;

    @TableField(value = "ACTORS")
    private String actors;

    @TableField(value = "\"STATUS\"")
    private BigDecimal status;

    @TableField(value = "STOP_RESON")
    private String stopReson;

    @TableField(value = "START_PROCESS_STATUS")
    private BigDecimal startProcessStatus;

    @TableField(value = "START_PROCESS_INSTANCE_ID")
    private String startProcessInstanceId;

    @TableField(value = "PRIMARY_PROCESS_STATUS")
    private BigDecimal primaryProcessStatus;

    @TableField(value = "PRIMARY_PROCESS_INSTANCE_ID")
    private String primaryProcessInstanceId;

    @TableField(value = "CREATE_USER")
    private String createUser;

    @TableField(value = "CREATE_TIME")
    private Date createTime;

    @TableField(value = "UPDATE_USER")
    private String updateUser;

    @TableField(value = "UPDATE_TIME")
    private Date updateTime;

    @TableField(value = "CANCEL_FLAG")
    private BigDecimal cancelFlag;

    @TableField(value = "COMMENTS")
    private String comments;

    @TableField(value = "PRO_NO")
    private String proNo;

    @TableField(value = "PRIMARY_SELECT_IDS")
    private String primarySelectIds;

    @TableField(value = "STOP_PROCESS_STATUS")
    private BigDecimal stopProcessStatus;

    @TableField(value = "STOP_PROCESS_INSTANCE_ID")
    private String stopProcessInstanceId;

    @TableField(value = "STOP_STATUS")
    private BigDecimal stopStatus;

    @TableField(value = "QUOTATION_NUMBER")
    private BigDecimal quotationNumber;

    @TableField(value = "SHOW_RANKING")
    private BigDecimal showRanking;

    @TableField(value = "SHOW_LOWPRICE")
    private BigDecimal showLowprice;

    public static final String COL_ID = "ID";

    public static final String COL_PRO_NAME = "PRO_NAME";

    public static final String COL_PUBLISH_DATE = "PUBLISH_DATE";

    public static final String COL_PRO_TYPE = "PRO_TYPE";

    public static final String COL_PLAN_EXEC_ID = "PLAN_EXEC_ID";

    public static final String COL_PLAN_BATCH_NOS = "PLAN_BATCH_NOS";

    public static final String COL_PLAN_BATCH_NAMES = "PLAN_BATCH_NAMES";

    public static final String COL_SETTLEMENT_METHOD = "SETTLEMENT_METHOD";

    public static final String COL_PUR_CONDITION = "PUR_CONDITION";

    public static final String COL_DESCRIPTION = "DESCRIPTION";

    public static final String COL_CONTACT = "CONTACT";

    public static final String COL_CONTACT_PHONE = "CONTACT_PHONE";

    public static final String COL_CONTACT_TEL = "CONTACT_TEL";

    public static final String COL_CONTACT_TEL_EXTENSION = "CONTACT_TEL_EXTENSION";

    public static final String COL_SUPPLIER_SELECT_IDS = "SUPPLIER_SELECT_IDS";

    public static final String COL_ENQUIRY_START_DATE = "ENQUIRY_START_DATE";

    public static final String COL_ENQUIRY_END_DATE = "ENQUIRY_END_DATE";

    public static final String COL_ENTRY_START_DATE = "ENTRY_START_DATE";

    public static final String COL_ENTRY_END_DATE = "ENTRY_END_DATE";

    public static final String COL_BIDDING_START_DATE = "BIDDING_START_DATE";

    public static final String COL_BIDDING_END_DATE = "BIDDING_END_DATE";

    public static final String COL_SHOW_END_DATE = "SHOW_END_DATE";

    public static final String COL_DEPOSIT = "DEPOSIT";

    public static final String COL_BID_FEE = "BID_FEE";

    public static final String COL_BID_SERVICE_FEE = "BID_SERVICE_FEE";

    public static final String COL_SPONSOR = "SPONSOR";

    public static final String COL_ACTORS = "ACTORS";

    public static final String COL_STATUS = "STATUS";

    public static final String COL_STOP_RESON = "STOP_RESON";

    public static final String COL_START_PROCESS_STATUS = "START_PROCESS_STATUS";

    public static final String COL_START_PROCESS_INSTANCE_ID = "START_PROCESS_INSTANCE_ID";

    public static final String COL_PRIMARY_PROCESS_STATUS = "PRIMARY_PROCESS_STATUS";

    public static final String COL_PRIMARY_PROCESS_INSTANCE_ID = "PRIMARY_PROCESS_INSTANCE_ID";

    public static final String COL_CREATE_USER = "CREATE_USER";

    public static final String COL_CREATE_TIME = "CREATE_TIME";

    public static final String COL_UPDATE_USER = "UPDATE_USER";

    public static final String COL_UPDATE_TIME = "UPDATE_TIME";

    public static final String COL_CANCEL_FLAG = "CANCEL_FLAG";

    public static final String COL_COMMENTS = "COMMENTS";

    public static final String COL_PRO_NO = "PRO_NO";

    public static final String COL_PRIMARY_SELECT_IDS = "PRIMARY_SELECT_IDS";

    public static final String COL_STOP_PROCESS_STATUS = "STOP_PROCESS_STATUS";

    public static final String COL_STOP_PROCESS_INSTANCE_ID = "STOP_PROCESS_INSTANCE_ID";

    public static final String COL_STOP_STATUS = "STOP_STATUS";

    public static final String COL_QUOTATION_NUMBER = "QUOTATION_NUMBER";

    public static final String COL_SHOW_RANKING = "SHOW_RANKING";

    public static final String COL_SHOW_LOWPRICE = "SHOW_LOWPRICE";
}
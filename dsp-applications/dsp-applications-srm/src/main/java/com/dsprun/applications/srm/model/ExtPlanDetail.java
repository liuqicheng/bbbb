package com.dsprun.applications.srm.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "ext_plan_detail")
public class ExtPlanDetail {
    @TableId(value = "plan_batch_no", type = IdType.INPUT)
    private String planBatchNo;

    //@TableId(value = "material_code", type = IdType.INPUT)
    @TableField(value = "material_code")
    private String materialCode;

    @TableField(value = "material_name")
    private String materialName;

    @TableField(value = "spn_typ")
    private String spnTyp;

    /**
     * 单位需要单位的id和名称的对应关系表
     */
    @TableField(value = "unit")
    private BigDecimal unit;

    /**
     * 数量
     */
    @TableField(value = "amount")
    private BigDecimal amount;

    @TableField(value = "manufacturer")
    private String manufacturer;

    @TableField(value = "arrival_date")
    private Date arrivalDate;

    /**
     * 采购周期
     */
    @TableField(value = "pur_cycle")
    private BigDecimal purCycle;

    @TableField(value = "sale_contract_no")
    private String saleContractNo;

    /**
     * 单包装数量
     */
    @TableField(value = "package_amount")
    private BigDecimal packageAmount;

    @TableField(value = "pro_name")
    private String proName;

    @TableField(value = "po_num")
    private String poNum;

    public static final String COL_PLAN_BATCH_NO = "plan_batch_no";

    public static final String COL_MATERIAL_CODE = "material_code";

    public static final String COL_MATERIAL_NAME = "material_name";

    public static final String COL_SPN_TYP = "spn_typ";

    public static final String COL_UNIT = "unit";

    public static final String COL_AMOUNT = "amount";

    public static final String COL_MANUFACTURER = "manufacturer";

    public static final String COL_ARRIVAL_DATE = "arrival_date";

    public static final String COL_PUR_CYCLE = "pur_cycle";

    public static final String COL_SALE_CONTRACT_NO = "sale_contract_no";

    public static final String COL_PACKAGE_AMOUNT = "package_amount";

    public static final String COL_PRO_NAME = "pro_name";

    public static final String COL_PO_NUM = "po_num";
}
package com.dsprun.applications.srm.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "ext_plan")
public class ExtPlan {
    @TableId(value = "plan_batch_no", type = IdType.INPUT)
    private String planBatchNo;

    @TableField(value = "plan_date")
    private Date planDate;

    @TableField(value = "plan_name")
    private String planName;

    @TableField(value = "applicant")
    private String applicant;

    @TableField(value = "plan_org_name")
    private String planOrgName;

    @TableField(value = "demand_org_name")
    private String demandOrgName;

    @TableField(value = "description")
    private String description;

    @TableField(value = "pusher")
    private String pusher;

    /**
     * 状态0:未认领 1：已认领
     */
    @TableField(value = "\"status\"")
    private BigDecimal status;

    @TableField(value = "insert_user")
    private String insertUser;

    @TableField(value = "insert_time")
    private Date insertTime;

    @TableField(value = "update_user")
    private String updateUser;

    @TableField(value = "update_time")
    private Date updateTime;

    /**
     * 删除flag0:有效 1：逻辑删除
     */
    @TableField(value = "cancel_flag")
    private BigDecimal cancelFlag;

    @TableField(value = "comments")
    private String comments;

    public static final String COL_PLAN_BATCH_NO = "plan_batch_no";

    public static final String COL_PLAN_DATE = "plan_date";

    public static final String COL_PLAN_NAME = "plan_name";

    public static final String COL_APPLICANT = "applicant";

    public static final String COL_PLAN_ORG_NAME = "plan_org_name";

    public static final String COL_DEMAND_ORG_NAME = "demand_org_name";

    public static final String COL_DESCRIPTION = "description";

    public static final String COL_PUSHER = "pusher";

    public static final String COL_STATUS = "status";

    public static final String COL_INSERT_USER = "insert_user";

    public static final String COL_INSERT_TIME = "insert_time";

    public static final String COL_UPDATE_USER = "update_user";

    public static final String COL_UPDATE_TIME = "update_time";

    public static final String COL_CANCEL_FLAG = "cancel_flag";

    public static final String COL_COMMENTS = "comments";
}
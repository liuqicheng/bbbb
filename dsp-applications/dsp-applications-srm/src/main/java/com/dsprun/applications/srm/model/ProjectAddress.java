package com.dsprun.applications.srm.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "PROJECT_ADDRESS")
public class ProjectAddress {
    /**
     * id自增长
     */
    @TableId(value = "ID", type = IdType.INPUT)
    private Integer id;

    /**
     * 项目Id
     */
    @TableField(value = "PRO_ID")
    private Integer proId;

    /**
     * 地址类型 1：收货地址 2：直发现场
     */
    @TableField(value = "\"TYPE\"")
    private BigDecimal type;

    /**
     * 区域 ID
     */
    @TableField(value = "REGION_ID")
    private Integer regionId;

    /**
     * 详细信息
     */
    @TableField(value = "DETAILED_ADDRESS")
    private String detailedAddress;

    /**
     * 添加人
     */
    @TableField(value = "CREATE_USER")
    private String createUser;

    /**
     * 添加时间
     */
    @TableField(value = "CREATE_TIME")
    private Date createTime;

    /**
     * 修改人
     */
    @TableField(value = "UPDATE_USER")
    private String updateUser;

    /**
     * 修改日期
     */
    @TableField(value = "UPDATE_TIME")
    private Date updateTime;

    /**
     * 删除flag0:有效 1：逻辑删除
     */
    @TableField(value = "CANCEL_FLAG")
    private BigDecimal cancelFlag;

    /**
     * 备注
     */
    @TableField(value = "COMMENTS")
    private String comments;

    public static final String COL_ID = "ID";

    public static final String COL_PRO_ID = "PRO_ID";

    public static final String COL_TYPE = "TYPE";

    public static final String COL_REGION_ID = "REGION_ID";

    public static final String COL_DETAILED_ADDRESS = "DETAILED_ADDRESS";

    public static final String COL_CREATE_USER = "CREATE_USER";

    public static final String COL_CREATE_TIME = "CREATE_TIME";

    public static final String COL_UPDATE_USER = "UPDATE_USER";

    public static final String COL_UPDATE_TIME = "UPDATE_TIME";

    public static final String COL_CANCEL_FLAG = "CANCEL_FLAG";

    public static final String COL_COMMENTS = "COMMENTS";
}
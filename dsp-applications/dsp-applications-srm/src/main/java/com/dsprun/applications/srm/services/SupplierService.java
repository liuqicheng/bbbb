package com.dsprun.applications.srm.services;

import java.util.HashMap;
import java.util.List;
import java.util.function.Supplier;

public interface SupplierService {
    List<Supplier> findSupplierByStandardFlag(Integer standardFlag);
}

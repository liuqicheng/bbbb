package com.dsprun.applications.srm.services;

import com.dsprun.applications.srm.model.Area;

import java.util.List;

public interface AreaService {
    List<Area> findAllProvincesAOBean();

    List<Area> findAllChildrenAOBean(Integer id);

}

package com.dsprun.applications.srm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dsprun.applications.srm.model.PurchasingOrganization;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Zhao Jun
 * 2020/11/1 19:54
 */
@Mapper
public interface PurchasingOrganizationMapper extends BaseMapper<PurchasingOrganization> {
}
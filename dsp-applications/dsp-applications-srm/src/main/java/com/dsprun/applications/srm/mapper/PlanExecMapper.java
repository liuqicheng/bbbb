package com.dsprun.applications.srm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dsprun.applications.srm.model.PlanExec;
import com.dsprun.applications.srm.vo.PlanExecVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PlanExecMapper extends BaseMapper<PlanExec> {
    List<PlanExecVO> selectExecAll();

    PlanExec findPlanExecById(String planExecId);

    PlanExecVO findPlanExecByExecId(Integer planExecId);
}
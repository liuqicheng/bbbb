package com.dsprun.applications.srm.client;

import feign.Headers;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Map;

@FeignClient(
        name = "dsp-applications-srm",
        url = "${dsp.applications.SrmService}"
)
@Headers({"Accept: application/json"})
public interface PlanmgrServiceClient {

    @RequestMapping(value = "/planmgr/api/getPlans", method = RequestMethod.POST)
    @Headers("Content-Type: application/json;charset=utf-8;")
    List<Map> getPlans();
}

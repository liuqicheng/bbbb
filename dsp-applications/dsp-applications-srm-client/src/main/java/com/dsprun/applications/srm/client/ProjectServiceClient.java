package com.dsprun.applications.srm.client;

import feign.Headers;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(
        name = "dsp-applications-srm",
        url = "${dsp.applications.SrmService}"
)
@Headers({"Accept: application/json"})
public interface ProjectServiceClient {

}

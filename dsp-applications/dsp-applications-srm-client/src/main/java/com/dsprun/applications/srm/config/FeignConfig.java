package com.dsprun.applications.srm.config;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * Feign配置
 * 使用FeignClient进行服务间调用，传递headers信息
 */
@Configuration
public class FeignConfig implements RequestInterceptor {


    @Autowired
    private HttpServletRequest request;


    @Override
    public void apply(RequestTemplate requestTemplate) {
        //ServletRequestAttributes attributes = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        //HttpServletRequest request = attributes.getRequest();

        //添加token
        requestTemplate.header("header_token", this.getToken());
        requestTemplate.header("user_id",this.getUserId());
    }

    private String getToken(){
        Cookie[] cookies = request.getCookies();
        for(Cookie cookie : cookies){
            if(cookie.getName().equals("header_token")){
                return cookie.getValue();
            }
        }
        return "";
    }

    private String getUserId(){
        Cookie[] cookies = request.getCookies();
        for(Cookie cookie : cookies){
            if(cookie.getName().equals("user_id")){
                return cookie.getValue();
            }
        }
        return "";
    }
}
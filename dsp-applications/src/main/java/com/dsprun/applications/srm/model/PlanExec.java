package com.dsprun.applications.srm.model;

import com.oracle.webservices.internal.api.databinding.DatabindingMode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * plan_exec
 * @author 
 */
public class PlanExec implements Serializable {
    /**
     * id自增长
     */
    private Integer id;

    /**
     * 计划批次号 (多个逗号分隔)
     */
    private String planBatchNos;

    /**
     * 计划名称 (多个逗号分隔)
     */
    private String planBatchNames;

    /**
     * 计划区分0：erp 1：sys
     */
    private BigDecimal planType;

    /**
     * 合并标识0:无 1：已合并 2:合并后
     */
    private BigDecimal unionFlag;

    /**
     * 合并数据来源
     */
    private String unionFrom;

    /**
     * 需求公司名称
     */
    private String demandCompName;

    /**
     * 需求公司 ID
     */
    private String demandCompId;

    /**
     * 需求部门名称 (多个逗号分隔)
     */
    private String demandOrgNames;

    /**
     * 需求部门 ID (多个逗号分隔)
     */
    private String demandOrgIds;

    /**
     * 状态0：待发起 1：已发起
     */
    private BigDecimal status;

    /**
     * 采购形式1 直接采购 2 询价采购 3 竞价采购 4 公开招标 5邀请招标 6 框架协议
     */
    private BigDecimal buyType;

    /**
     * 采购人
     */
    private String buyer;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改人
     */
    private String updateUser;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 删除flag0:有效 1：逻辑删除
     */
    private BigDecimal cancelFlag;

    /**
     * 备注
     */
    private String comments;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPlanBatchNos() {
        return planBatchNos;
    }

    public void setPlanBatchNos(String planBatchNos) {
        this.planBatchNos = planBatchNos;
    }

    public String getPlanBatchNames() {
        return planBatchNames;
    }

    public void setPlanBatchNames(String planBatchNames) {
        this.planBatchNames = planBatchNames;
    }

    public BigDecimal getPlanType() {
        return planType;
    }

    public void setPlanType(BigDecimal planType) {
        this.planType = planType;
    }

    public BigDecimal getUnionFlag() {
        return unionFlag;
    }

    public void setUnionFlag(BigDecimal unionFlag) {
        this.unionFlag = unionFlag;
    }

    public String getUnionFrom() {
        return unionFrom;
    }

    public void setUnionFrom(String unionFrom) {
        this.unionFrom = unionFrom;
    }

    public String getDemandCompName() {
        return demandCompName;
    }

    public void setDemandCompName(String demandCompName) {
        this.demandCompName = demandCompName;
    }

    public String getDemandCompId() {
        return demandCompId;
    }

    public void setDemandCompId(String demandCompId) {
        this.demandCompId = demandCompId;
    }

    public String getDemandOrgNames() {
        return demandOrgNames;
    }

    public void setDemandOrgNames(String demandOrgNames) {
        this.demandOrgNames = demandOrgNames;
    }

    public String getDemandOrgIds() {
        return demandOrgIds;
    }

    public void setDemandOrgIds(String demandOrgIds) {
        this.demandOrgIds = demandOrgIds;
    }

    public BigDecimal getStatus() {
        return status;
    }

    public void setStatus(BigDecimal status) {
        this.status = status;
    }

    public BigDecimal getBuyType() {
        return buyType;
    }

    public void setBuyType(BigDecimal buyType) {
        this.buyType = buyType;
    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public BigDecimal getCancelFlag() {
        return cancelFlag;
    }

    public void setCancelFlag(BigDecimal cancelFlag) {
        this.cancelFlag = cancelFlag;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}
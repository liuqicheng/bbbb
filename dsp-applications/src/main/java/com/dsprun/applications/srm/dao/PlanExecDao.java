package com.dsprun.applications.srm.dao;

import com.dsprun.applications.srm.model.PlanExec;

public interface PlanExecDao {
    int deleteByPrimaryKey(Integer id);

    int insert(PlanExec record);

    int insertSelective(PlanExec record);

    PlanExec selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(PlanExec record);

    int updateByPrimaryKey(PlanExec record);
}